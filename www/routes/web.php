<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'SeriesController@index')->name('series.index');

Route::get('series/{seriesSlug}/episodes/{episodeSlug}', 'SeriesEpisodesController@show')->name('seriesEpisodes.show');

Route::post('stripe/webhook', 'WebhookController@handleWebhook')->name('webhook');

Route::middleware('auth')->group(function () {
    Route::get('/account', 'AccountsController@index')->name('accounts.index')->middleware('auth');
    Route::post('/account', 'AccountsController@store')->name('accounts.store')->middleware('auth');

    Route::get('/payouts', 'PayoutsController@index')->name('payouts.index')
        ->middleware('providersOnly');

    Route::get('/subscriptions/renew', 'SubscriptionsController@create')->name('subscriptions.create')
        ->middleware('customersOnly');

    Route::get('/subscriptions/delete', 'SubscriptionsController@destroy')->name('subscriptions.destroy')
        ->middleware('customersOnly');
});
