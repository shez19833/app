@extends('layouts.app')

@section('content')
    @foreach($payouts as $payout)
        Payout for Month: {{ $payout->created_at->subMonth() }} <br/>
        Paid at: {{ $payout->paid_at }} <br/>
        Total: {{ $payout->total }} <br/>
        <br/>
        <br/>
    @endforeach

    {{ $payouts->links() }}
@endsection
