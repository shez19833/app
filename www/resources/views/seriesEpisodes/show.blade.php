@extends('layouts.app')

@section('content')

    <div class="container" style="height: 400px;">
        <div class="row">
            <div class="col-md-12">
                <h1>{{ $currentEpisode->name }}</h1>
                {{ $currentEpisode->description }}
            </div>
        </div>
        <div class="row">
                @auth
                    <div class="col-md-9">
                        <video controls>
                            <source src="{{ asset('storage/video.mp4') }}"
                                    type="video/webm">

                            {{-- <source src="{{ $currentEpisode->video }}"
                            type="video/webm">--}}

                            Sorry, your browser doesn't support embedded videos.
                        </video>
                    </div>
                @endauth

                @guest
                    <div class="col-md-9 bg-dark text-white text-center h-100">
                        <br/>
                        <h2>Access Denied</h2>
                        <br/><br/>
                        <p>Please log in before you can view this episode</p>
                    </div>
                @endauth
            <div class="col-md-3">
                <div class="list-group overflow-auto h-100">
                    @foreach($series->episodes as $episode)
                        <a href="{{ $episode->url }}" class="list-group-item list-group-item-action
                            @if($currentEpisode->id == $episode->id) active @endif
                            ">
                            {{ $episode->name }} - {{ formatDuration($episode->duration) }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>
                    {{ $series->name }}
                </h2>
                {{ $series->description }}<br/>
            </div>
        </div>
    </div>
@endsection
