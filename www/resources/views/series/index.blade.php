@extends('layouts.app')

@section('content')
    <form class="form-inline" action="" method="get">
        {{--<label for="providers">Providers</label>--}}
        {{--<select id="providers" class="mb-2 mr-sm-2" name="providers[]" multiple>--}}
            {{--@foreach($providers as $provider)--}}
                {{--<option value="{{ $provider->slug }}">{{ $provider->name }}</option>--}}
            {{--@endforeach--}}
        {{--</select> <br/>--}}

        <label for="text" class="mb-2 mr-sm-2">Text</label>
        <input id="text" class="mb-2 mr-sm-2" type="text" name="text" placeholder="search Title / Description"/><br/>

        {{--<label>Tags</label>--}}
        {{--<select name="tags[]" class="mb-2 mr-sm-2" multiple>--}}
            {{--@foreach($tags as $tag)--}}
                {{--<option value="{{ $tag->name }}">{{ $tag->name }}</option>--}}
            {{--@endforeach--}}
        {{--</select> <br/>--}}

        <input type="submit" class="btn btn-primary mb-2" name="search">
    </form>

        <div class="row text-center">
        @foreach($series as $aSeries)
            <div class="col-md-3">
                <div class="card mb-5 text-center">
                    <div class="card-header">
                        <h5 class="card-title">{{ $aSeries->name }}</h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <a href="{{ $aSeries->route_url }}" class="btn btn-primary">Watch</a>
                        </div>

                        <div>
                            Duration: {{ formatDuration($aSeries->duration) }}
                        </div>

                        <div>
                            @foreach($aSeries->tags as $tag)
                                <a class="btn btn-secondary bg-layout" href="{{ $tag->url }}">{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">{{ $aSeries->updated_at->diffForHumans() }}</small>
                    </div>
                </div>
            </div>
        @endforeach
        </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{ $series->links() }}
            </div>
        </div>
    </div>

@endsection
