@extends('layouts.app')

@section('content')
    {{ $series->name }} <br/>
    {{ $series->description }}<br/>

    <br/>
    <br/>

    @foreach($episodes as $episode)
        {{ $episode->name }}<br/>
        {{ $episode->description }}<br/>
        {{ $episode->duration }}<br/>

        <br/>
        <br/>
    @endforeach
@endsection
