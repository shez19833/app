@extends('layouts.app')

@section('content')

    <form action="" method="post">
        @csrf
        <fieldset>
            <legend>Account</legend>

            <label>Name</label>
            <input type="text" name="name" value="{{ \Auth::user()->name }}"><br/>

            <label>Email</label>
            <input type="email" name="email" value="{{ \Auth::user()->email }}"><br/>

        </fieldset>

        <fieldset>
            <legend>Passwords</legend>

            <label>Old Password</label>
            <input type="password" name="old_password"><br/>

            <label>New Password</label>
            <input type="password" name="new_password"><br/>

            <label>New Password Confirm</label>
            <input type="password" name="new_password_confirmation"><br/>
        </fieldset>

        <fieldset>
            <legend>Newsletter</legend>

            <label>Would you like to be subscribed to our newsletter?</label>
            <input type="radio" name="wants_newsletter" value="1" @if(\Auth::user()->wants_newsletter == 1) checked @endif> Yes
            <input type="radio" name="wants_newsletter" value="0" @if(\Auth::user()->wants_newsletter == 0) checked @endif> No

        </fieldset>

        @if(\Auth::user()->isACustomer())
            <fieldset>
                <legend>Card</legend>

                <label>Existing Card:</label> {{ \Auth::user()->card_brand }} {{ str_repeat('**** ', 3) }} {{ \Auth::user()->card_last_four }}<br/>

                <input id="cardholder-name" type="text">

                <div id="card-element"></div>
                <button class="btn btn-secondary" id="card-button" data-secret="<?= $intent->client_secret ?>">
                    Change Card
                </button>
                <div id="card-errors">

                </div>
            </fieldset>

            <br/><br/>

            @if (\Auth::user()->subscribed('main') && ! \Auth::user()->subscription('main')->onGracePeriod())
                <a href="{{ route('subscriptions.destroy') }}">Cancel</a>
            @else
                <a href="{{ route('subscriptions.create') }}">Renew</a>
            @endif

            <input type="hidden" name="payment_method" id="payment_method">
        @endif

        @if(\Auth::user()->isAProvider())
            <label>Name</label>
            <input type="text" name="provider_name" value="{{ \Auth::user()->provider->name }}"><br/>

            <label>description</label>
            <input type="text" name="provider_description" value="{{ \Auth::user()->provider->description }}"><br/>

            <label>URL</label>
            <input type="text" name="provider_url" value="{{ \Auth::user()->provider->url }}"><br/>
        @endif

        <input type="submit" value="save">
    </form>

@endsection
