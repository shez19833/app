@extends('layouts.app')

@section('content')
    @foreach($providers as $provider)
        {{ $provider->name }} <br/>
        {{ $provider->description }}<br/>
        <a href="{{ $provider->route_url }}">Show All Series</a><br/>

        <br/>
        <br/>
    @endforeach
@endsection
