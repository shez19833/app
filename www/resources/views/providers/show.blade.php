@extends('layouts.app')

@section('content')
    {{ $provider->name }} <br/>
    {{ $provider->description }}<br/>

    <br/>
    <br/>

    @foreach($series as $aSeries)
        {{ $aSeries->name }}<br/>
        {{ $aSeries->description }}<br/>
        <a href="{{ $aSeries->route_url }}">Show Episodes</a><br/>

        <br/>
        <br/>
    @endforeach
@endsection
