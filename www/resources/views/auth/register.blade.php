@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id="payment-form">
                        @csrf

                        <div class="form-group row">
                            <label for="type_id" class="col-md-4 col-form-label text-md-right">{{ __('Are you registering as?') }}</label>

                            <div class="col-md-6">
                                <input id="type_id" type="radio" class="form-control" name="type_id" required value="1"> Provider
                                <input id="type_id" type="radio" class="form-control" name="type_id" required value="2"> Customer
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="wants_newsletter" class="col-md-4 col-form-label text-md-right">{{ __('Would you like to receive newsletter?') }}</label>

                            <div class="col-md-6">
                                <input id="wants_newsletter" type="radio" class="form-control" name="wants_newsletter" required value="0"> No, thanks
                                <input id="wants_newsletter" type="radio" class="form-control" name="wants_newsletter" required value="1"> Yes, please
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cardholder-name" class="col-md-4 col-form-label text-md-right">{{ __('Name on Your Card') }}</label>

                            <div class="col-md-6">
                                <input id="cardholder-name" type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="card-element" class="col-md-4 col-form-label text-md-right">{{ __('Card details') }}</label>

                            <div class="col-md-6">
                                <div id="card-element"></div>
                                <button class="btn btn-secondary" id="card-button" data-secret="<?= $intent->client_secret ?>">
                                    Add Card
                                </button>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="card-errors" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <div id="card-errors">

                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <input type="hidden" name="payment_method" id="payment_method">
                                <button class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
