<?php

use App\Models\Episode;
use App\Models\Series;
use Illuminate\Database\Seeder;

class EpisodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Series::all()->each(function ($series){
            $limit = random_int(4, 11);

            for($i = 1; $i <= $limit; $i++) {
                factory(Episode::class)
                    ->create([
                        'series_id' => $series->id,
                        'name' => 'Episode ' . $i,
                        'slug' => 'episode-' . $i . '-' . $series->id,
                        'description' => 'This episode will make you better at this skill.'
                    ]);
            }
        });
    }
}

