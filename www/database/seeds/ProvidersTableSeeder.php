<?php

use App\Models\Provider;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $providers = [
            [
                'name' => 'Laracasts',
                'url' => 'https://laracasts.com'
            ],
            [
                'name' => 'Codecourse',
                'url' => 'https://codecourse.com'
            ],
            [
                'name' => 'Pluralsight',
                'url' => 'https://pluralsight.com'
            ],
        ];

        foreach ($providers as $provider) {
            $user =  factory(User::class)
                ->states('provider')
                ->create([
                    'name' => $provider['name'],
                    'email' => Str::slug($provider['name']). '@email.com',
                ]);

            factory(Provider::class)
                ->create([
                    'user_id' => $user->id,
                    'name' => $provider['name'],
                    'slug' => Str::slug($provider['name']),
                ]);
        }
    }
}
