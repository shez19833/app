<?php

use App\Models\Provider;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // @todo when we 'extract' series from providers
        // match their 'tags' with ours?

        $tags = [
            'php',
            'laravel',
            'testing',
            'sql',
            'no sql',

        ];

        foreach ($tags as $tag) {
            factory(Tag::class)->create([
                'name' => $tag,
                'slug' => Str::slug($tag)
            ]);
        }
    }
}
