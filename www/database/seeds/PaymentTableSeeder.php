<?php

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::customersOnly()
            ->get()
            ->each(function ($user){
                factory(Payment::class, 5)
                    ->states('paid')
                    ->create([
                        'user_id' => $user
                    ]);
        });


    }
}
