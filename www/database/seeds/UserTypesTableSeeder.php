<?php

use App\Models\UserType;
use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(UserType::class)->create(['name' => 'Provider']);
       factory(UserType::class)->create(['name' => 'Customer']);
    }
}
