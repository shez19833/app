<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(Setting::class)->create([
           'key' => 'Subscription Price',
           'value' => 1500,
       ]);

        factory(Setting::class)->create([
            'key' => 'Commission',
            'value' => 200,
        ]);
    }
}
