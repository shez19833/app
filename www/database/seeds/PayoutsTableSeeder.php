<?php

use App\Models\Payout;
use App\Models\User;
use Illuminate\Database\Seeder;

class PayoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::providersOnly()
            ->get()
            ->each(function ($user){
                factory(Payout::class, 5)
                    ->states('paid')
                    ->create([
                        'user_id' => $user
                    ]);
        });


    }
}
