<?php

use App\Models\Provider;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SeriesTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $tags = Tag::all('id');

        foreach (Series::all() as $aSeries) {
            $aSeries->tags()->sync($tags->random(random_int(1,3)));
        }
    }
}
