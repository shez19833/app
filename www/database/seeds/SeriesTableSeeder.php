<?php

use App\Models\Provider;
use App\Models\Series;
use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::all()->each(function ($provider){
           factory(Series::class, random_int(3, 6))->create([
               'provider_id' => $provider->id,
           ]);
        });
    }
}
