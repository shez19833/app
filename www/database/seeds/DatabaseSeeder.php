<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesTableSeeder::class);
        $this->call(ProvidersTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(TagsTableSeeder::class);

        $this->call(SeriesTableSeeder::class);
        $this->call(EpisodesTableSeeder::class);

        $this->call(SeriesTagsTableSeeder::class);

        $this->call(PayoutsTableSeeder::class);
        $this->call(PaymentTableSeeder::class);

        $this->call(SettingsTableSeeder::class);
    }
}
