<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Payment;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'total' => $faker->randomNumber(4),
        'user_id' => function() {
            return factory(User::class)->states('provider')->create();
        },
        'paid_at' => $faker->randomElement([null, $faker->dateTime]),
    ];
});

$factory->state(Payment::class, 'paid', function(Faker $faker){
    return [
        'paid_at' => $faker->dateTimeBetween('-10 months', '-1 month'),
    ];
});

$factory->state(Payment::class, 'lastMonth', function(Faker $faker){
    return [
        'paid_at' => $faker->dateTimeBetween('-1 month', '-1 month'),
    ];
});
