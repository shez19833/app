<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Setting;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Setting::class, function (Faker $faker) {
    return [
        'key' => $faker->word,
        'value' => $faker->word,
        'description' => $faker->sentence,
    ];
});
