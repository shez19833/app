<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Episode;
use App\Models\Series;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Episode::class, function (Faker $faker) {
    return [
        'series_id' => function() {
            return factory(Series::class)->create();
        },
        'name' => $faker->words(5, true),
        'slug' => $faker->slug,
        'video' => $faker->url . '?=' . Str::random(),
        'description' => $faker->sentences(3, true),
        'duration' => random_int(60, 240),
    ];
});
