<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Payout;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Payout::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)
                ->states('provider')
                ->create();
        },
        'total' => $faker->randomNumber(),
        'paid_at' => null,
        'period' => null,
    ];
});

$factory->state(Payout::class, 'paid', function(Faker $faker){
    return [
        'paid_at' => $faker->dateTime(),
        'period' => $faker->dateTime(),
    ];
});

$factory->state(Payout::class, 'lastMonth', function(Faker $faker){
   return [
       'paid_at' => $faker->dateTimeBetween('-1 month', '-1 month'),
       'period' => $faker->dateTimeBetween('-2 month', '-2 month'),
   ];
});

$factory->state(Payout::class, 'twoMonthsAgo', function(Faker $faker){
    return [
        'paid_at' => $faker->dateTimeBetween('-2 month', '-2 month'),
        'period' => $faker->dateTimeBetween('-3 month', '-3 month'),
    ];
});
