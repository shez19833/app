<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Laravel\Cashier\Subscription;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->states('customer')->create();
        },
        'name' => 'main',
        'stripe_id' => Str::random(),
        'stripe_status' => 'active',
        'stripe_plan' => Str::random(10),
        'quantity' => 1,
        'trial_ends_at' => null,
        'ends_at' => null,
    ];
});
