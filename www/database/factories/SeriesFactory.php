<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Provider;
use App\Models\Series;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Series::class, function (Faker $faker) {

    $languages = ['php', 'java', 'c++', 'python', 'ruby', '.net', 'VB', 'Kotlin', 'Android', 'Swift'];
    $categories = ['testing', 'oop', 'patterns', 'solid principles', 'API', 'refactoring', 'security'];
    $skills = ['beginner', 'intermediate', 'expert'];

    $language = $faker->randomElement($languages);
    $category = $faker->randomElement($categories);
    $skill = $faker->randomElement($skills);

    $name = $language . ' ' . $category . ' ' . $skill;

    return [
        'provider_id' => function() {
            return factory(Provider::class)->create();
        },
        'name' => $name,
        'description' => "This series will teach you all about $category in $language. This series is suitable for $skill",
        'slug' => Str::slug($name),
        'cover_image' => $faker->url,
    ];
});
