<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeriesTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series_tags', function (Blueprint $table) {
            $table->unsignedBigInteger('series_id');
            $table->unsignedMediumInteger('tag_id');

            $table->unique(['series_id', 'tag_id']);

            $table->foreign('series_id')
                ->references('id')
                ->on('series');

            $table->foreign('tag_id')
                ->references('id')
                ->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series_tags');
    }
}
