<?php

namespace App\Http\Middleware;

use Closure;

class CustomersOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && \Auth::user()->isACustomer()) {
            return $next($request);
        }

        return redirect('/');
    }
}
