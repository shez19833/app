<?php

namespace App\Http\Controllers;

use App\Events\StripeIncomingPayment;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{
    /**
     * Handle invoice payment succeeded.
     * @tested
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleChargeSucceeded($payload)
    {
        info('Incoming Payload for handleChargeSucceeded', $payload);
        StripeIncomingPayment::dispatch($payload);

        return $this->successMethod();
    }
}
