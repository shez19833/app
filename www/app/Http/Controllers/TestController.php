<?php
namespace App\Http\Controllers;
use App\Console\Commands\ScrapingServices\CodecourseScraper;
use App\Console\Commands\ScrapingServices\LaracastScraper;
use App\Models\Episode;
use App\Models\Provider;
use GuzzleHttp\Client;

class TestController extends Controller
{
    public function test2(CodecourseScraper $scraper)
    {
        $html = $this->getEpisodesPart($this->getEpisodesExample());

        preg_match_all('~title:"(.*?)",~', $html, $titles);
        preg_match_all('~,hls:"(.*?)"}~', $html, $url);
        preg_match_all('~{duration:(.*?),~', $html, $durations);

        try {
            Episode::create([
                'name' => $titles[1][0],
                'series_id' => 204,
                'duration' => $durations[1][0] ?? null,
                'video' => $url[1][0] ?? null,
            ]);
        } catch (\Exception $e) {
            info($e->getMessage());
        }
    }


    private function getSeriesExample()
    {
        return '
           {"data":[{"id":366,"title":"Setting up Doctrine in Slim 4","slug":"setting-up-doctrine-in-slim-4","description":"Doctrine is a powerful ORM (Object Relational Mapper) and works perfectly for Slim projects. In this course, we\'ll run through the setup in Slim 4.","free":false,"ongoing":false,"type":"course","difficulty":"intermediate","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":22}},"created_at_human":"5 days ago","created_at":"2019-10-08","subjects":{"data":[{"id":10,"title":"Slim","description":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","description_short":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","slug":"slim","color":"#000000","version":"4"}]}},{"id":365,"title":"League Container with Slim 4","slug":"league-container-with-slim-4","description":"Slim comes with no container by default, and using League Container gives you the power of service providers, autowiring and more.","free":false,"ongoing":false,"type":"course","difficulty":"intermediate","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":55}},"created_at_human":"6 days ago","created_at":"2019-10-07","subjects":{"data":[{"id":10,"title":"Slim","description":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","description_short":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","slug":"slim","color":"#000000","version":"4"}]}},{"id":364,"title":"Build a Devspiration app","slug":"build-a-devspiration-app","description":"It\'s a simple app concept, but you\'ll learn Flutter routing, how to create and use an app drawer, make API requests and more.","free":false,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":1,"minutes":28}},"created_at_human":"1 week ago","created_at":"2019-10-01","subjects":{"data":[{"id":25,"title":"Flutter","description":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","description_short":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","slug":"flutter","color":"#000000","version":"1.9"},{"id":8,"title":"Laravel","description":"The PHP framework for web artisans.","description_short":"The PHP framework for web artisans.","slug":"laravel","color":"#000000","version":"5.8"}]}},{"id":363,"title":"API Requests with Flutter","slug":"api-requests-with-flutter","description":"Get started making API requests in Flutter. We\'ll cover simple requests, working with models and extracting HTTP logic to a dedicated web service class.","free":false,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":47}},"created_at_human":"1 week ago","created_at":"2019-10-01","subjects":{"data":[{"id":25,"title":"Flutter","description":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","description_short":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","slug":"flutter","color":"#000000","version":"1.9"}]}},{"id":362,"title":"Build a shopping list with Flutter","slug":"build-a-shopping-list-with-flutter","description":"Build yourself a simple shopping list and learn about rendering a ListView and handling basic state in Flutter along the way.","free":false,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":29}},"created_at_human":"2 weeks ago","created_at":"2019-09-23","subjects":{"data":[{"id":25,"title":"Flutter","description":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","description_short":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","slug":"flutter","color":"#000000","version":"1.9"}]}},{"id":361,"title":"Your first Flutter app","slug":"your-first-flutter-app","description":"Build a simple counter app with Flutter and get familiar with basic concepts like Widgets and state.","free":false,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":28}},"created_at_human":"3 weeks ago","created_at":"2019-09-16","subjects":{"data":[{"id":25,"title":"Flutter","description":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","description_short":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","slug":"flutter","color":"#000000","version":"1.9"}]}},{"id":360,"title":"Setting up Flutter","slug":"setting-up-flutter","description":"Your guide to getting Flutter installed so you can start building native iOS and Android apps.","free":true,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":15}},"created_at_human":"3 weeks ago","created_at":"2019-09-16","subjects":{"data":[{"id":25,"title":"Flutter","description":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","description_short":"Flutter is a UI toolkit for building native applications for mobile, web, and desktop.","slug":"flutter","color":"#000000","version":"1.9"}]}},{"id":359,"title":"Views with Slim 4","slug":"views-with-slim-4","description":"Routes quickly get messy without views. Let\'s set up Slim 4 with the Twig view component.","free":false,"ongoing":false,"type":"course","difficulty":"beginner","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":24}},"created_at_human":"3 weeks ago","created_at":"2019-09-16","subjects":{"data":[{"id":10,"title":"Slim","description":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","description_short":"A PHP micro-framework that helps you quickly write simple, powerful web applications and APIs.","slug":"slim","color":"#000000","version":"4"}]}},{"id":358,"title":"Vue Roles and Permissions","slug":"vue-roles-and-permissions","description":"A guide to using roles and permissions in your Vue\/Nuxt app, directly from your API.","free":false,"ongoing":true,"type":"course","difficulty":"intermediate","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":46}},"created_at_human":"1 month ago","created_at":"2019-09-10","subjects":{"data":[{"id":21,"title":"Nuxt.js","description":"A framework for creating Vue.js applications.","description_short":"A framework for creating Vue.js applications.","slug":"nuxt-js","color":"#000000","version":"2.9.2"},{"id":11,"title":"Vue.js","description":"The progressive JavaScript framework.","description_short":"The progressive JavaScript framework.","slug":"vue-js","color":"#000000","version":"2"},{"id":8,"title":"Laravel","description":"The PHP framework for web artisans.","description_short":"The PHP framework for web artisans.","slug":"laravel","color":"#000000","version":"5.8"}]}},{"id":357,"title":"Deploying Nuxt","slug":"deploying-nuxt","description":"How to deploy your Nuxt app with Laravel Forge, set up an Nginx reverse proxy, and deploy your API alongside it, on the same domain, to prevent additional HEAD requests from your client.","free":false,"ongoing":false,"type":"course","difficulty":"intermediate","new":false,"has_new_parts":false,"duration":{"formatted":{"hours":0,"minutes":47}},"created_at_human":"1 month ago","created_at":"2019-09-09","subjects":{"data":[{"id":21,"title":"Nuxt.js","description":"A framework for creating Vue.js applications.","description_short":"A framework for creating Vue.js applications.","slug":"nuxt-js","color":"#000000","version":"2.9.2"}]}}],"meta":{"pagination":{"total":264,"count":10,"per_page":10,"current_page":1,"total_pages":27,"links":{"next":"https:\/\/codecourse.com\/api\/courses?page=2"}}}}
        ';
    }


    private function getEpisodesPart(string $html)
    {
        preg_match('~parts:\{parts:\[(.*)errors:~', $html, $matches);

        return $matches[1];
    }

    private function getEpisodesExample()
    {
        return '
        <!doctype html>
<html data-n-head-ssr>
  <head >
    <title>Introduction and demo | Codecourse</title><meta data-n-head="ssr" charset="utf-8"><meta data-n-head="ssr" name="viewport" content="width=device-width, initial-scale=1"><meta data-n-head="ssr" name="twitter:title" content="Build a classified ads site - Introduction and demo"><meta data-n-head="ssr" name="twitter:card" content="summary_large_image"><meta data-n-head="ssr" name="twitter:widgets:new-embed-design" content="on"><meta data-n-head="ssr" property="og:image" content="https://codecourse.com/api/courses/build-a-classified-ads-site/card"><meta data-n-head="ssr" property="twitter:image:src" content="https://codecourse.com/api/courses/build-a-classified-ads-site/card"><link data-n-head="ssr" rel="icon" type="image/x-icon" href="/favicon.ico"><link data-n-head="ssr" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&amp;display=swap"><link data-n-head="ssr" rel="manifest" href="/_nuxt/manifest.bf25d7b2.json"><link rel="preload" href="/_nuxt/adbd7b1bfdfa6da3e75f.js" as="script"><link rel="preload" href="/_nuxt/48944ce208f6223a3464.js" as="script"><link rel="preload" href="/_nuxt/f90413075625e2f255c2.js" as="script"><link rel="preload" href="/_nuxt/ea79bf38b0d305fbf77d.css" as="style"><link rel="preload" href="/_nuxt/03946dd60fbf59c02fa1.js" as="script"><link rel="preload" href="/_nuxt/1d5e7e6b853dc5ea1e28.css" as="style"><link rel="preload" href="/_nuxt/a785e2d819e4ff1ea969.js" as="script"><link rel="preload" href="/_nuxt/b226734dba3fe4f93805.js" as="script"><link rel="stylesheet" href="/_nuxt/ea79bf38b0d305fbf77d.css"><link rel="stylesheet" href="/_nuxt/1d5e7e6b853dc5ea1e28.css">
  </head>
  <body >
    <div data-server-rendered="true" id="__nuxt"><!----><div id="__layout"><div><div class="h-full w-full bg-gray-900 pt-82px"><header class="fixed w-full z-50 top-0"><div class="flex items-center justify-between bg-white h-82px"><div class="h-full flex items-center"><div class="px-6 flex items-center h-full"><a href="/" class="flex items-center nuxt-link-active"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000" class="w-10 h-10"><path fill="#57CCFF" d="M500.002 0C223.86 0 0 223.856 0 499.995 0 776.142 223.86 1000 500.002 1000 776.142 1000 1000 776.142 1000 499.995 1000 223.855 776.14 0 500.002 0zm-.004 750c-138.073 0-250-111.934-250-250.005 0-138.07 111.927-250 250-250 138.072 0 250.002 111.93 250.002 250S638.07 750 499.998 750z"></path><path fill="#03A6FF" d="M754.208 79.805c-12.32-14.677-26.515-25.547-42.852-33.026.25.11-.284-.045-.04.07h.005l.036-.07C647.152 16.787 575.546 0 500.002 0 223.86 0 0 223.856 0 499.995 0 700.6 118.155 873.585 288.654 953.227l.033-.078c-.005 0-.005-.007-.01-.007-62.565-29.18-89.628-103.543-60.46-166.112 29.137-62.465 103.337-89.545 165.84-60.575-85.097-39.883-144.06-126.265-144.06-226.46 0-138.07 111.927-250 250-250 37.474 0 72.99 8.31 104.893 23.084 42.787 20.258 95.278 15.245 133.917-17.177 52.877-44.38 59.77-123.217 15.4-176.098z"></path></svg></a></div> <nav class="items-center hidden lg:flex relative top-auto left-auto bg-white w-auto"><ul class="list-none p-0 m-0 flex items-center w-auto"><li class="mr-4 relative w-auto"><a href="/library" class="p-3 pr-0 text-lg font-bold text-gray-500 hover:text-gray-700">
								Library
							</a></li> <li class="mr-4 relative w-auto"><a href="/subjects" class="p-3 pr-0 text-lg font-bold text-gray-500 hover:text-gray-700">
								Subjects
							</a></li> <li class="mr-4 relative w-auto"><a href="/paths" class="p-3 pr-0 text-lg font-bold text-gray-500 hover:text-gray-700">
								Paths
							</a></li> <!----></ul></nav></div> <div class="flex items-center h-full px-6"><div class="h-full hidden lg:flex items-center justify-center mr-6"><a href="/auth/signin?redirect=%2Fwatch%2Fbuild-a-classified-ads-site" class="p-3 pr-0 text-lg font-bold text-gray-500 hover:text-gray-700">
						Sign in
					</a></div> <div class="h-full hidden lg:flex items-center justify-center"><a href="/pro" class="font-semibold px-5 py-4 relative text-base inline-block rounded text-center text-lg font-bold !py-3 bg-blue-500 text-white h:text-white">Join pro</a></div> <!----> <!----> <div class="h-full flex lg:hidden items-center justify-center ml-4 relative"><a href="#"><div class="bg-gray-300 h-12 w-12 rounded flex items-center justify-center flex-col relative"><span class="block w-6/12 h-1 bg-gray-500 rounded mb-1"></span> <span class="block w-6/12 h-1 bg-gray-500 rounded mb-1"></span> <span class="block w-6/12 h-1 bg-gray-500 rounded"></span></div></a></div></div></div> <!----></header> <div class="bg-gray-300 p-6 flex lg:items-center justify-between flex-wrap lg:flex-no-wrap"><h4 class="font-bold text-lg mb-2 lg:mb-0 mr-6">
        Build a classified ads site <span class="text-gray-500">/</span> <span class="font-normal">
            Introduction and demo
        </span></h4> <div class="w-full lg:w-3/12"><div class="flex items-center flex-wrap md:flex-no-wrap"><span class="font-semibold text-gray-600">
        0 complete
    </span> <div class="h-6px rounded bg-white flex-grow mx-2"><div class="w-full bg-green-600 h-6px rounded" style="width:0%;"></div></div> <span class="font-semibold text-gray-600">
        44 parts
    </span></div></div></div> <div class="flex p-4 flex-wrap lg:flex-no-wrap"><div class="w-full lg:w-9/12 mr-0 lg:mr-4"><div class="player"><div id="player"></div></div></div> <div class="w-full lg:w-3/12"><div class="pt-0 overflow-y-auto relative mt-4 lg:mt-0 h-400px lg:h-full"><div class="bg-white rounded overflow-hidden absolute h-full w-full pt-82px"><div class="absolute top-0 left-0 h-82px w-full px-6 border-b-2 border-gray-200 flex items-center justify-between flex-wrap bg-white z-40"><div><label for="autoplay" class="flex items-center"><input type="checkbox" id="autoplay" class="hidden"> <div class="relative w-10 h-5 bg-gray-300 rounded-full mr-2 before:empty before:h-4 before:w-4 before:rounded-full before:absolute before:left-0 before:ml-2px before:bg-white before:mt-2px"></div> <span>
            Autoplay
        </span></label></div> <a href="/watch/build-a-classified-ads-site?part=221-installing-laravel-and-setting-up" class="font-semibold px-5 py-4 relative text-base inline-block rounded text-center !py-3 bg-gray-400 text-blue-900 h:text-blue-900">Next &rarr;</a></div> <div class="w-full h-full overflow-y-auto"><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200 bg-gray-100"><a href="/watch/build-a-classified-ads-site?part=221-introduction-and-demo" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                1. Introduction and demo
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                09:34
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <!----> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-installing-laravel-and-setting-up" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                2. Installing Laravel and setting up
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                03:14
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-introducing-nested-sets" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                3. Introducing nested sets
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                03:06
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-fake-email" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                4. Fake email
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                01:53
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-laravel-debugbar" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                5. Laravel DebugBar
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                02:17
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-breaking-up-the-default-template" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                6. Breaking up the default template
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                02:31
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-populating-areas" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                7. Populating areas
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                12:41
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-area-selection" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                8. Area selection
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                08:11
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-choosing-and-persisting-an-area" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                9. Choosing and persisting an area
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                15:18
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-populating-categories" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                10. Populating categories
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                05:44
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-listing-categories" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                11. Listing categories
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                08:54
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-setting-up-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                12. Setting up listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                15:15
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-showing-listings-in-categories" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                13. Showing listings in categories
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                19:03
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-listing-count-for-categories" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                14. Listing count for categories
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                06:07
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-showing-a-listing" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                15. Showing a listing
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                13:37
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-setting-up-favourites" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                16. Setting up favourites
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                07:46
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-adding-a-favourite-listing" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                17. Adding a favourite listing
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                11:50
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-showing-favourites" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                18. Showing favourites
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                11:28
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-deleting-favourites" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                19. Deleting favourites
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                04:46
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-ordering-and-eager-loading-improvements" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                20. Ordering and eager loading improvements
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                06:40
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-quick-flash-messages" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                21. Quick flash messages
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                04:44
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-setting-up-listing-views" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                22. Setting up listing views
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                05:21
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-logging-views" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                23. Logging views
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                08:48
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-recently-viewed-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                24. Recently viewed listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                08:25
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-showing-the-listing-view-count" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                25. Showing the listing view count
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                06:17
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-contact-form-and-validation" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                26. Contact form and validation
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                07:05
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-sending-the-email" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                27. Sending the email
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                11:30
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-usable-areas-and-categories" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                28. Usable areas and categories
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                04:07
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-listing-form-and-storing" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                29. Listing form and storing
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                21:47
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-handling-validation" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                30. Handling validation
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                14:34
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-editing-a-listing" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                31. Editing a listing
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                12:22
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-editing-live-listings-improvements" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                32. Editing live listings improvements
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                02:08
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-starting-the-payment-journey" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                33. Starting the payment journey
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                11:25
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-braintree-integration" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                34. Braintree integration
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                20:12
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-processing-the-payment" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                35. Processing the payment
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                06:39
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-handling-free-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                36. Handling free listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                03:23
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-unpublished-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                37. Unpublished listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                19:00
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-published-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                38. Published listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                02:38
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-deleting-a-listing" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                39. Deleting a listing
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                05:33
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-indexing-listings" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                40. Indexing listings
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                15:09
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-basic-listing-search" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                41. Basic listing search
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                14:22
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-the-full-search-experience" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                42. The full search experience
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                24:38
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-form-and-validation" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                43. Form and validation
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                18:53
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div><div class="group part relative flex items-center justify-between py-5 px-6 border-b-2 border-gray-200"><a href="/watch/build-a-classified-ads-site?part=221-sending-emails" class="absolute w-full h-full top-0 left-0 z-30"></a> <div class="flex justify-between flex-col w-full"><div class="flex items-center justify-between mb-4"><h3 class="text-lg">
                44. Sending emails
            </h3> <div class="text-gray-500 flex justify-end items-center font-bold">
                09:28
            </div></div> <div class="flex items-center justify-between flex-wrap"><ul class="list-none p-0 m-0 flex items-center"><!----> <li><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 z-50 !mb-0 bg-gray-700 text-gray-100">
    Pro

    </div></li> <!----></ul> <div class="flex items-center z-40 opacity-25 group-hover:opacity-100 group-hover:scale-10"><!----> <!----> </div></div></div> </div></div></div></div></div></div> <div class="flex px-4 pt-16 bg-white flex-wrap lg:flex-no-wrap"><div class="px-4 md:px-16 w-full lg:w-9/12 mr-0 lg:mr-4 lg:border-r-2 border-gray-200"><div class="flex flex-wrap md:flex-no-wrap mb-16"><div class="w-full flex pb-16 border-b-2 border-gray-200 flex-wrap md:flex-no-wrap"><div class="w-full md:w-6/12 lg:w-5/12 md:mr-6"><h1 class="text-3xl mb-6 font-bold">
							Build a classified ads site
						</h1> <ul class="list-none flex flex-wrap m-0 p-0 mb-6"><li class="mr-4 text-gray-500 flex items-center mb-4 font-semibold"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" class="mr-2 w-5 h-5 fill-current text-gray-500"><path d="M12 22a10 10 0 110-20 10 10 0 010 20zm0-2a8 8 0 100-16 8 8 0 000 16zm1-8.41l2.54 2.53a1 1 0 01-1.42 1.42L11.3 12.7a1 1 0 01-.3-.7V8a1 1 0 012 0v3.59z"></path></svg> 
            7 hours
         
            8 mins
        </li> <li class="mr-4 text-gray-500 flex items-center mb-4 font-semibold"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" class="mr-2 mr-2 w-5 h-5 fill-current text-gray-500"><path d="M17 4h2a2 2 0 012 2v14a2 2 0 01-2 2H5a2 2 0 01-2-2V6c0-1.1.9-2 2-2h2V3a1 1 0 112 0v1h6V3a1 1 0 012 0v1zm-2 2H9v1a1 1 0 11-2 0V6H5v4h14V6h-2v1a1 1 0 01-2 0V6zm4 6H5v8h14v-8z"></path></svg> <span title="2017-02-13">
            Released 2 years ago
        </span></li></ul> <!----></div> <div class="w-full md:w-6/12 lg:w-8/12"><p class="mb-6">
							A site that allows users to browse, create and pay for advertisements. Think Craigslist. We\'ll cover Eloquent techniques, nested sets for flexibility, payment processing and more.
						</p> <div class="flex flex-no-wrap"><a href="/subjects/laravel"><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 bg-gray-300 text-gray-600">
    Laravel

    </div></a><a href="/subjects/php"><div class="inline-block text-sm font-semibold rounded px-3 py-2 mr-2 mb-2 bg-gray-300 text-gray-600">
    PHP

    </div></a> <!----> <!----> <!----></div></div></div></div> <!----> <div class="mb-16"> <!----></div></div> <div class="w-full lg:w-3/12 overflow-y-scroll px-6 pb-16"><div class="mb-8"><h4 class="mb-4 font-bold uppercase text-gray-500">
    Links
</h4> <div><ul class="list-none m-0 p-0"><li class="mb-2"><a href="https://eggerapps.at/postico/" target="_blank" rel="noopener">
        Postico
    </a></li><li class="mb-2"><a href="https://github.com/lazychaser/laravel-nestedset" target="_blank" rel="noopener">
        lazychaser/laravel-nestedset
    </a></li><li class="mb-2"><a href="http://mailtrap.io" target="_blank" rel="noopener">
        Mailtrap
    </a></li><li class="mb-2"><a href="http://braintreepayments.com/codecourse" target="_blank" rel="noopener">
        Braintree
    </a></li><li class="mb-2"><a href="https://developers.braintreepayments.com/" target="_blank" rel="noopener">
        Braintree developer documentation
    </a></li><li class="mb-2"><a href="https://github.com/braintree/braintree_php" target="_blank" rel="noopener">
        Braintree PHP client library
    </a></li><li class="mb-2"><a href="https://github.com/barryvdh/laravel-debugbar" target="_blank" rel="noopener">
        Laravel Debugbar
    </a></li><li class="mb-2"><a href="https://www.algolia.com/" target="_blank" rel="noopener">
        Algolia
    </a></li><li class="mb-2"><a href="https://github.com/algolia/algoliasearch-client-php" target="_blank" rel="noopener">
        Algolia PHP client
    </a></li><li class="mb-2"><a href="https://github.com/algolia/algoliasearch-client-javascript" target="_blank" rel="noopener">
        Algolia Javascript Client
    </a></li><li class="mb-2"><a href="https://github.com/algolia/autocomplete.js" target="_blank" rel="noopener">
        autocomplete.js
    </a></li><li class="mb-2"><a href="https://github.com/braintree/braintree-web" target="_blank" rel="noopener">
        braintree/braintree-web
    </a></li></ul></div></div> <div class="mb-8 !mb-6"><h4 class="mb-4 font-bold uppercase text-gray-500">
    Resources
</h4> <p class="m-0">
					No resources for this course
				</p></div> <div class="mb-8 !mb-6"><a href="/watch/build-a-classified-ads-site" class="font-semibold px-5 py-4 relative text-base inline-block rounded text-center w-full nuxt-link-exact-active nuxt-link-active bg-gray-400 text-blue-900 h:text-blue-900">Full course code</a></div></div></div></div></div></div></div><script>window.__NUXT__=(function(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,_,$,aa,ab){return {layout:"default",data:[{}],error:b,state:{newsletters:{newsletters:[]},alert:{alert:b},checkout:{plans:[]},comments:{comments:[],pagination:b,loading:a},form:{submitting:b},library:{courses:[],pagination:{}},auth:{user:b,strategy:"local"},notifications:{notifications:[]},parts:{parts:[{id:O,title:"Introduction and demo",slug:p,free:f,order:c,duration:"09:34",new:a,file:{data:{duration:574,stream:{data:{id:1338,provider_id:"338684335",hls:"https:\u002F\u002Fplayer.vimeo.com\u002Fexternal\u002F338684335.m3u8?s=4990d7cf2d5b71076eca3d5d912007e9dd8c45ff&oauth2_token_id=1212323593"}},download:{data:{id:1420}}}},user:{data:{can_access:f}}},{id:Q,title:"Installing Laravel and setting up",slug:"221-installing-laravel-and-setting-up",free:a,order:2,duration:"03:14",new:a,file:{data:{duration:194,stream:{data:{id:1339,provider_id:b,hls:b}},download:{data:{id:1419}}}},user:{data:{can_access:a}}},{id:S,title:"Introducing nested sets",slug:"221-introducing-nested-sets",free:a,order:3,duration:"03:06",new:a,file:{data:{duration:186,stream:{data:{id:1340,provider_id:b,hls:b}},download:{data:{id:1418}}}},user:{data:{can_access:a}}},{id:U,title:"Fake email",slug:"221-fake-email",free:a,order:4,duration:"01:53",new:a,file:{data:{duration:113,stream:{data:{id:1341,provider_id:b,hls:b}},download:{data:{id:1417}}}},user:{data:{can_access:a}}},{id:W,title:"Laravel DebugBar",slug:"221-laravel-debugbar",free:a,order:_,duration:"02:17",new:a,file:{data:{duration:137,stream:{data:{id:1342,provider_id:b,hls:b}},download:{data:{id:1416}}}},user:{data:{can_access:a}}},{id:l,title:"Breaking up the default template",slug:"221-breaking-up-the-default-template",free:a,order:6,duration:"02:31",new:a,file:{data:{duration:151,stream:{data:{id:1343,provider_id:b,hls:b}},download:{data:{id:1415}}}},user:{data:{can_access:a}}},{id:m,title:"Populating areas",slug:"221-populating-areas",free:a,order:n,duration:"12:41",new:a,file:{data:{duration:761,stream:{data:{id:2615,provider_id:b,hls:b}},download:{data:{id:1414}}}},user:{data:{can_access:a}}},{id:o,title:"Area selection",slug:"221-area-selection",free:a,order:d,duration:"08:11",new:a,file:{data:{duration:491,stream:{data:{id:O,provider_id:b,hls:b}},download:{data:{id:1413}}}},user:{data:{can_access:a}}},{id:q,title:"Choosing and persisting an area",slug:"221-choosing-and-persisting-an-area",free:a,order:9,duration:"15:18",new:a,file:{data:{duration:918,stream:{data:{id:Q,provider_id:b,hls:b}},download:{data:{id:1412}}}},user:{data:{can_access:a}}},{id:r,title:"Populating categories",slug:"221-populating-categories",free:a,order:10,duration:"05:44",new:a,file:{data:{duration:344,stream:{data:{id:S,provider_id:b,hls:b}},download:{data:{id:1411}}}},user:{data:{can_access:a}}},{id:s,title:"Listing categories",slug:"221-listing-categories",free:a,order:11,duration:"08:54",new:a,file:{data:{duration:534,stream:{data:{id:U,provider_id:b,hls:b}},download:{data:{id:1410}}}},user:{data:{can_access:a}}},{id:t,title:"Setting up listings",slug:"221-setting-up-listings",free:a,order:12,duration:"15:15",new:a,file:{data:{duration:915,stream:{data:{id:W,provider_id:b,hls:b}},download:{data:{id:1409}}}},user:{data:{can_access:a}}},{id:u,title:"Showing listings in categories",slug:"221-showing-listings-in-categories",free:a,order:13,duration:"19:03",new:a,file:{data:{duration:1143,stream:{data:{id:l,provider_id:b,hls:b}},download:{data:{id:1408}}}},user:{data:{can_access:a}}},{id:v,title:"Listing count for categories",slug:"221-listing-count-for-categories",free:a,order:14,duration:"06:07",new:a,file:{data:{duration:367,stream:{data:{id:m,provider_id:b,hls:b}},download:{data:{id:1407}}}},user:{data:{can_access:a}}},{id:w,title:"Showing a listing",slug:"221-showing-a-listing",free:a,order:15,duration:"13:37",new:a,file:{data:{duration:817,stream:{data:{id:o,provider_id:b,hls:b}},download:{data:{id:1406}}}},user:{data:{can_access:a}}},{id:x,title:"Setting up favourites",slug:"221-setting-up-favourites",free:a,order:16,duration:"07:46",new:a,file:{data:{duration:466,stream:{data:{id:q,provider_id:b,hls:b}},download:{data:{id:1405}}}},user:{data:{can_access:a}}},{id:y,title:"Adding a favourite listing",slug:"221-adding-a-favourite-listing",free:a,order:17,duration:"11:50",new:a,file:{data:{duration:710,stream:{data:{id:r,provider_id:b,hls:b}},download:{data:{id:1404}}}},user:{data:{can_access:a}}},{id:z,title:"Showing favourites",slug:"221-showing-favourites",free:a,order:18,duration:"11:28",new:a,file:{data:{duration:688,stream:{data:{id:s,provider_id:b,hls:b}},download:{data:{id:1403}}}},user:{data:{can_access:a}}},{id:A,title:"Deleting favourites",slug:"221-deleting-favourites",free:a,order:19,duration:"04:46",new:a,file:{data:{duration:286,stream:{data:{id:t,provider_id:b,hls:b}},download:{data:{id:1402}}}},user:{data:{can_access:a}}},{id:B,title:"Ordering and eager loading improvements",slug:"221-ordering-and-eager-loading-improvements",free:a,order:20,duration:"06:40",new:a,file:{data:{duration:400,stream:{data:{id:u,provider_id:b,hls:b}},download:{data:{id:1401}}}},user:{data:{can_access:a}}},{id:ab,title:"Quick flash messages",slug:"221-quick-flash-messages",free:a,order:21,duration:"04:44",new:a,file:{data:{duration:284,stream:{data:{id:v,provider_id:b,hls:b}},download:{data:{id:1400}}}},user:{data:{can_access:a}}},{id:D,title:"Setting up listing views",slug:"221-setting-up-listing-views",free:a,order:22,duration:"05:21",new:a,file:{data:{duration:321,stream:{data:{id:w,provider_id:b,hls:b}},download:{data:{id:1399}}}},user:{data:{can_access:a}}},{id:E,title:"Logging views",slug:"221-logging-views",free:a,order:23,duration:"08:48",new:a,file:{data:{duration:528,stream:{data:{id:x,provider_id:b,hls:b}},download:{data:{id:1398}}}},user:{data:{can_access:a}}},{id:F,title:"Recently viewed listings",slug:"221-recently-viewed-listings",free:a,order:24,duration:"08:25",new:a,file:{data:{duration:505,stream:{data:{id:y,provider_id:b,hls:b}},download:{data:{id:1397}}}},user:{data:{can_access:a}}},{id:G,title:"Showing the listing view count",slug:"221-showing-the-listing-view-count",free:a,order:25,duration:"06:17",new:a,file:{data:{duration:377,stream:{data:{id:z,provider_id:b,hls:b}},download:{data:{id:1396}}}},user:{data:{can_access:a}}},{id:H,title:"Contact form and validation",slug:"221-contact-form-and-validation",free:a,order:26,duration:"07:05",new:a,file:{data:{duration:425,stream:{data:{id:A,provider_id:b,hls:b}},download:{data:{id:1395}}}},user:{data:{can_access:a}}},{id:I,title:"Sending the email",slug:"221-sending-the-email",free:a,order:27,duration:"11:30",new:a,file:{data:{duration:690,stream:{data:{id:B,provider_id:b,hls:b}},download:{data:{id:1394}}}},user:{data:{can_access:a}}},{id:J,title:"Usable areas and categories",slug:"221-usable-areas-and-categories",free:a,order:28,duration:"04:07",new:a,file:{data:{duration:247,stream:{data:{id:ab,provider_id:b,hls:b}},download:{data:{id:1393}}}},user:{data:{can_access:a}}},{id:K,title:"Listing form and storing",slug:"221-listing-form-and-storing",free:a,order:29,duration:"21:47",new:a,file:{data:{duration:1307,stream:{data:{id:D,provider_id:b,hls:b}},download:{data:{id:1392}}}},user:{data:{can_access:a}}},{id:L,title:"Handling validation",slug:"221-handling-validation",free:a,order:30,duration:"14:34",new:a,file:{data:{duration:874,stream:{data:{id:E,provider_id:b,hls:b}},download:{data:{id:1391}}}},user:{data:{can_access:a}}},{id:M,title:"Editing a listing",slug:"221-editing-a-listing",free:a,order:31,duration:"12:22",new:a,file:{data:{duration:742,stream:{data:{id:F,provider_id:b,hls:b}},download:{data:{id:1390}}}},user:{data:{can_access:a}}},{id:N,title:"Editing live listings improvements",slug:"221-editing-live-listings-improvements",free:a,order:32,duration:"02:08",new:a,file:{data:{duration:128,stream:{data:{id:G,provider_id:b,hls:b}},download:{data:{id:1389}}}},user:{data:{can_access:a}}},{id:k,title:"Starting the payment journey",slug:"221-starting-the-payment-journey",free:a,order:33,duration:"11:25",new:a,file:{data:{duration:685,stream:{data:{id:H,provider_id:b,hls:b}},download:{data:{id:P}}}},user:{data:{can_access:a}}},{id:g,title:"Braintree integration",slug:"221-braintree-integration",free:a,order:34,duration:"20:12",new:a,file:{data:{duration:1212,stream:{data:{id:I,provider_id:b,hls:b}},download:{data:{id:R}}}},user:{data:{can_access:a}}},{id:h,title:"Processing the payment",slug:"221-processing-the-payment",free:a,order:35,duration:"06:39",new:a,file:{data:{duration:399,stream:{data:{id:J,provider_id:b,hls:b}},download:{data:{id:T}}}},user:{data:{can_access:a}}},{id:i,title:"Handling free listings",slug:"221-handling-free-listings",free:a,order:36,duration:"03:23",new:a,file:{data:{duration:203,stream:{data:{id:K,provider_id:b,hls:b}},download:{data:{id:V}}}},user:{data:{can_access:a}}},{id:j,title:"Unpublished listings",slug:"221-unpublished-listings",free:a,order:37,duration:"19:00",new:a,file:{data:{duration:1140,stream:{data:{id:L,provider_id:b,hls:b}},download:{data:{id:X}}}},user:{data:{can_access:a}}},{id:Y,title:"Published listings",slug:"221-published-listings",free:a,order:38,duration:"02:38",new:a,file:{data:{duration:158,stream:{data:{id:M,provider_id:b,hls:b}},download:{data:{id:Z}}}},user:{data:{can_access:a}}},{id:Z,title:"Deleting a listing",slug:"221-deleting-a-listing",free:a,order:39,duration:"05:33",new:a,file:{data:{duration:333,stream:{data:{id:N,provider_id:b,hls:b}},download:{data:{id:Y}}}},user:{data:{can_access:a}}},{id:X,title:"Indexing listings",slug:"221-indexing-listings",free:a,order:40,duration:"15:09",new:a,file:{data:{duration:909,stream:{data:{id:k,provider_id:b,hls:b}},download:{data:{id:j}}}},user:{data:{can_access:a}}},{id:V,title:"Basic listing search",slug:"221-basic-listing-search",free:a,order:41,duration:"14:22",new:a,file:{data:{duration:862,stream:{data:{id:g,provider_id:b,hls:b}},download:{data:{id:i}}}},user:{data:{can_access:a}}},{id:T,title:"The full search experience",slug:"221-the-full-search-experience",free:a,order:42,duration:"24:38",new:a,file:{data:{duration:1478,stream:{data:{id:h,provider_id:b,hls:b}},download:{data:{id:h}}}},user:{data:{can_access:a}}},{id:R,title:"Form and validation",slug:"221-form-and-validation",free:a,order:43,duration:"18:53",new:a,file:{data:{duration:1133,stream:{data:{id:i,provider_id:b,hls:b}},download:{data:{id:g}}}},user:{data:{can_access:a}}},{id:P,title:"Sending emails",slug:"221-sending-emails",free:a,order:e,duration:"09:28",new:a,file:{data:{duration:568,stream:{data:{id:j,provider_id:b,hls:b}},download:{data:{id:k}}}},user:{data:{can_access:a}}}],pagination:{total:e,count:e,per_page:1000,current_page:c,total_pages:c,links:{}},user:{favorites:[],completions:[]}},paths:{path:b},subject:{subject:{},courses:[],pagination:{}},validation:{errors:{}},watch:{course:{id:221,title:"Build a classified ads site",slug:"build-a-classified-ads-site",description:"A site that allows users to browse, create and pay for advertisements. Think Craigslist. We\'ll cover Eloquent techniques, nested sets for flexibility, payment processing and more.",free:a,ongoing:a,type:"course",difficulty:"beginner",new:a,has_new_parts:a,duration:{formatted:{hours:n,minutes:d}},created_at_human:"2 years ago",created_at:"2017-02-13",subjects:{data:[{id:d,title:"Laravel",description:$,description_short:$,slug:"laravel",color:aa,version:b},{id:c,title:"PHP",description:C,description_short:C,slug:"php",color:aa,version:b}]},user:{data:{last_watched_slug:b}},links:{data:[{id:115,title:"Postico",uri:"https:\u002F\u002Feggerapps.at\u002Fpostico\u002F"},{id:200,title:"lazychaser\u002Flaravel-nestedset",uri:"https:\u002F\u002Fgithub.com\u002Flazychaser\u002Flaravel-nestedset"},{id:141,title:"Mailtrap",uri:"http:\u002F\u002Fmailtrap.io"},{id:70,title:"Braintree",uri:"http:\u002F\u002Fbraintreepayments.com\u002Fcodecourse"},{id:71,title:"Braintree developer documentation",uri:"https:\u002F\u002Fdevelopers.braintreepayments.com\u002F"},{id:73,title:"Braintree PHP client library",uri:"https:\u002F\u002Fgithub.com\u002Fbraintree\u002Fbraintree_php"},{id:199,title:"Laravel Debugbar",uri:"https:\u002F\u002Fgithub.com\u002Fbarryvdh\u002Flaravel-debugbar"},{id:_,title:"Algolia",uri:"https:\u002F\u002Fwww.algolia.com\u002F"},{id:132,title:"Algolia PHP client",uri:"https:\u002F\u002Fgithub.com\u002Falgolia\u002Falgoliasearch-client-php"},{id:190,title:"Algolia Javascript Client",uri:"https:\u002F\u002Fgithub.com\u002Falgolia\u002Falgoliasearch-client-javascript"},{id:198,title:"autocomplete.js",uri:"https:\u002F\u002Fgithub.com\u002Falgolia\u002Fautocomplete.js"},{id:201,title:"braintree\u002Fbraintree-web",uri:"https:\u002F\u002Fgithub.com\u002Fbraintree\u002Fbraintree-web"}]},resources:{data:[]},codes:{data:[{id:168,title:"Classified ads site",free:a}]}},currentPartSlug:p,autoplay:a,user:{favorites:[],starts:[],saved:[]}}},serverRendered:f}}(false,null,1,8,44,true,1378,1379,1380,1381,1377,1350,1351,7,1352,"221-introduction-and-demo",1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1363,1364,"The most popular server side scripting language for the web.",1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1345,1388,1346,1387,1347,1386,1348,1385,1349,1384,1382,1383,5,"The PHP framework for web artisans.","#000000",1365));</script><script src="/_nuxt/adbd7b1bfdfa6da3e75f.js" defer></script><script src="/_nuxt/a785e2d819e4ff1ea969.js" defer></script><script src="/_nuxt/b226734dba3fe4f93805.js" defer></script><script src="/_nuxt/48944ce208f6223a3464.js" defer></script><script src="/_nuxt/f90413075625e2f255c2.js" defer></script><script src="/_nuxt/03946dd60fbf59c02fa1.js" defer></script>
  </body>
</html>

';
    }
}
