<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    /**
     * Show all series
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) : Renderable
    {
        $series = Series::with('provider', 'tags', 'firstEpisode')
            ->search($request->all())
            ->order($request->all())
            ->paginate();

        $providers = Provider::all();
        $tags = Tag::all();

        return view('series.index', compact('providers', 'series', 'tags'));
    }
}
