<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use App\Models\Provider;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class SeriesEpisodesController extends Controller
{
    /**
     * Show a series with episodes
     *
     * @param string $seriesSlug
     * @param String $episodeSlug
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(string $seriesSlug, String $episodeSlug) : Renderable
    {
        $series = Series::with('episodes')
            ->whereSlug($seriesSlug)
            ->firstOrFail();

        $currentEpisode = $series->episodes
            ->where('slug', $episodeSlug)
            ->first();

//        $previousEpisode = $series->episodes->where('id', $currentEpisode->id - 1)->first();
//        $nextEpisode = $series->episodes->where('id', $currentEpisode->id + 1)->first();

        return view('seriesEpisodes.show', compact(
            'series','currentEpisode', 'previousEpisode', 'nextEpisode'
        ));
    }
}
