<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;

class SubscriptionsController extends Controller
{
    /**
     * Resume a Subscription
     *
     * @return RedirectResponse
     */
    public function create() : RedirectResponse
    {
        \Auth::user()->subscription('main')->resume();

        return back()->with('success', 'Subscription reinstated');
    }

    /**
     * Cancel a Subscription
     *
     * @return RedirectResponse
     */
    public function destroy() : RedirectResponse
    {
        \Auth::user()->subscription('main')->cancel();

        return back()->with('success', 'Subscription cancelled');
    }
}
