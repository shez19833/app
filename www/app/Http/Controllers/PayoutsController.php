<?php

namespace App\Http\Controllers;

use App\Actions\UserUpdateAction;
use App\Http\Requests\AccountRequest;
use App\Models\Payout;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;

class PayoutsController extends Controller
{
    /**
     * Show user their account page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() : Renderable
    {
        $payouts = Payout::latest('id')
            ->paginate();

        return view('payouts.index', compact('payouts'));
    }
}
