<?php

namespace App\Http\Controllers;

use App\Actions\UserUpdateAction;
use App\Http\Requests\AccountRequest;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;

class AccountsController extends Controller
{
    /**
     * Show user their account page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() : Renderable
    {
        $intent = \Auth::user()->createSetupIntent();

        return view('accounts.index', compact('intent'));
    }

    /**
     * Update Users data
     *
     * @param AccountRequest $request
     * @param UserUpdateAction $action
     *
     * @return RedirectResponse
     *
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function store(AccountRequest $request, UserUpdateAction $action) : RedirectResponse
    {
        $action->handle(\Auth::user(), $request->all());

        return back()->with('success', 'Settings saved');
    }
}
