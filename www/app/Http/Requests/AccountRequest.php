<?php

namespace App\Http\Requests;

use App\Rules\PasswordStrength;
use App\Rules\VerifyPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore(\Auth::id()),
            ],
            'name' => 'nullable',
            'new_password' => [
                'nullable',
                'required_with:old_password',
                'confirmed',
            ],
            'old_password' => [
                'nullable',
                'bail',
                'required_with:new_password',
                new VerifyPassword()
            ],
            'wants_newsletter' => 'nullable|in:0,1',
        ];
    }
}
