<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordStrength implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // @todo remove this before going to production although it will still work!
        if(config('app.env') == 'local')
        {
            return true;
        }

        return (bool) preg_match("~^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[[:punct:]±§£]).{8,}$~", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please make sure password has minimum 8 characters, containing ONE Number, Punctuation, lowercase and uppercase letter';
    }
}
