<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Provider extends Model
{
    const CODECOURSE = 'Codecourse';
    const LARACASTS = 'Laracasts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'logo',
        'name',
        'slug',
        'url',
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        /*
         * @tested
         */
        static::saving(function ($model){
            $model->slug = Str::slug($model->name);
        });
    }

    /*
     * Accessors
     */
    
    public function getRouteUrlAttribute()
    {
        return route('providers.show', ['slug' => $this->slug]);
    }

    /*
     * Relations
     */

    /*
     * @tested
     */
    public function episodes()
    {
        return $this->hasManyThrough(Episode::class, Series::class);
    }

    public function series()
    {
        return $this->hasMany(Series::class);
    }
}
