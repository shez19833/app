<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'colour',
        'name',
        'slug',
    ];

    public $timestamps = false;

    public function getUrlAttribute()
    {
        return route('series.index', ['tags[]' => $this->name]);
    }
}
