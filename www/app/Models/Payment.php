<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'total',
        'paid_at',
    ];

    protected $casts = [
        'paid_at' => 'datetime',
        'period' => 'datetime',
    ];

    public $timestamps = false;
}
