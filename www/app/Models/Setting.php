<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const SUBSCRIPTION_PRICE = 'Subscription Price';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
        'description',
    ];

    public $timestamps = false;

    public static function get($key)
    {
        return self::where('key', $key)
            ->firstOrFail()
            ->value;
    }
}
