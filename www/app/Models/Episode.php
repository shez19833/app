<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'duration',
        'links',
        'name',
        'series_id',
        'slug',
        'video',
    ];

    public $timestamps = false;

    public function getUrlAttribute()
    {
        $this->loadMissing('series');

        return route('seriesEpisodes.show', [
            'seriesSlug' => $this->series->slug,
            'episodeSlug' => $this->slug,
        ]);
    }

    /*
     * Relationships
     */

    public function series()
    {
        return $this->belongsTo(Series::class);
    }

    public function watchedBy()
    {
        return $this->belongsToMany(User::class,'watched');
    }
}
