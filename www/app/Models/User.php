<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Billable, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'email_verified_at',
        'name',
        'password',
        'type_id',
        'wants_newsletter',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'wants_newsletter' => 'boolean',
    ];

    /*
     * Helper
     */

    public function isACustomer()
    {
        return $this->type_id == UserType::CUSTOMER;
    }

    public function isAProvider()
    {
        return $this->type_id == UserType::PROVIDER;
    }

    /*
     * Scope
     */

    public function scopeCustomersOnly($q)
    {
        return $q->whereTypeId(UserType::CUSTOMER);
    }

    public function scopeProvidersOnly($q)
    {
        return $q->whereTypeId(UserType::PROVIDER);
    }

    /*
     * Relations
     */

    public function provider()
    {
        return $this->hasOne(Provider::class);
    }

    public function watched()
    {
        return $this->belongsToMany(Episode::class,'watched')
            ->withPivot('watched_at');
    }
}
