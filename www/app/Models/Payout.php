<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'total',
        'paid_at',
        'period',
    ];

    protected $casts = [
        'paid_at' => 'datetime',
        'period' => 'datetime',
    ];
}
