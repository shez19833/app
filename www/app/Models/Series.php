<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cover_image',
        'description',
        'external_slug',
        'name',
        'provider_id',
        'slug',
    ];

    /*
     * Accessors
     */

    public function getRouteUrlAttribute()
    {
        $this->loadMissing('firstEpisode');

        return route('seriesEpisodes.show', [
            'seriesSlug' => $this->slug,
            'episodeSlug' => $this->firstEpisode->slug,
        ]);
    }

    public function getDurationAttribute()
    {
        return $this->episodes()->sum('duration');
    }

    /*
     * Scopes
     */

    public function scopeOrder($q, $params)
    {
        $params['order'] = $params['order'] ?? 'name';

        switch ($params['order']) {
            case 'recent':
                $q->orderBy('updated_at', 'desc');
                break;

            case 'name':
                $q->orderBy('name', 'asc');
                break;
        }
    }

    public function scopeSearch($q, $params)
    {
        $params = array_filter($params);

        if ( count($params) == 0 ) {
            return $q;
        }

        foreach ($params as $param => $value){
            switch ($param) {
                case 'providers':
                    $q->whereHas('provider', function ($q) use ($value) {
                        $q->whereIn('name', $value);
                    });
                    break;

                case 'text':
                    $q->where('name', 'like', '%' . $value .'%');
                    break;

                case 'tags':
                    $q->whereHas('tags', function ($q) use ($value) {
                        $q->whereIn('name', $value);
                    });
                    break;
            }
        }
    }

    /*
     * Relationships
     */

    public function firstEpisode()
    {
        return $this->hasOne(Episode::class)->oldest('id');
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'series_tags');
    }
}
