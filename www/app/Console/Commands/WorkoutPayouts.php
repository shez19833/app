<?php

namespace App\Console\Commands;

use App\Console\Commands\WorkoutStrategy\NoEpisodesWatchedStrategy;
use App\Console\Commands\WorkoutStrategy\SimilarAmountOfEpisodesWatchedStrategy;
use App\Models\Payment;
use App\Models\Payout;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class WorkoutPayouts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'z:workout-payouts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Workout Payouts for all Providers for the last month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // @todo do some validation ie if payout already exist for a user..
        // @todo unless we pass a flag to update?

        // Workout episodes watched per provider
        $providers = collect(DB::select(sprintf("
                select u.id user_id, sum(w.watched_count) episodes_watched
                from providers p
                 join series s on s.provider_id = p.id
                 join episodes e on e.series_id = s.id
                 join users u on u.id = p.user_id
                 left join (
                    select 
                      count(*) watched_count, w.episode_id
                    from watched w
                     where w.watched_at between '%s' and '%s'
                    group by w.episode_id
                  ) w ON w.episode_id = e.id
                 group by u.id, w.watched_count
        ", now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth())));

        $totalEpisodesWatched = $providers->sum('episodes_watched');
        $totalAmountPaid = Payment::whereRaw('paid_at between :date1 and :date2', [
                'date1' => now()->subMonth()->startOfMonth(),
                'date2' => now()->subMonth()->endOfMonth()
            ])->sum('total');

        // workout a strategy to use to workout the payouts depending on episodes watched
        $strategy = $this->workoutStrategy($totalEpisodesWatched);

        foreach ($providers as $provider) {
            // insert the row in the payouts
            Payout::create([
                'user_id' => $provider->user_id,
                'total' => $strategy->calculatePayoutFor($providers, $provider, $totalEpisodesWatched, $totalAmountPaid),
                'period' => now()->subMonth()->endOfMonth(),
            ]);
        }
    }

    public function workoutStrategy($totalEpisodesWatched)
    {
        return $totalEpisodesWatched == 0
            ? new NoEpisodesWatchedStrategy()
            : new SimilarAmountOfEpisodesWatchedStrategy();
    }
}
