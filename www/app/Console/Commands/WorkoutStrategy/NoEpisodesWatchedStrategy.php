<?php

namespace App\Console\Commands\WorkoutStrategy;

class NoEpisodesWatchedStrategy {

    public function calculatePayoutFor($providers, $provider, $totalEpisodesWatched, $totalAmountPaid)
    {
        return $totalAmountPaid / $providers->count();
    }
}
