<?php

namespace App\Console\Commands\WorkoutStrategy;


class SimilarAmountOfEpisodesWatchedStrategy {

    public function calculatePayoutFor($providers, $provider, $totalEpisodesWatched, $totalAmountPaid)
    {
        return $totalAmountPaid / $totalEpisodesWatched * $provider->episodes_watched;
    }
}
