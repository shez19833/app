<?php

namespace App\Console\Commands\ScrapingServices;

use App\Models\Episode;
use App\Models\Series;
use App\Models\Tag;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class CodecourseScraper {

    public $client;
    public $provider;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function handle($provider)
    {
        return;
        $this->provider = $provider;

        $this->scrapeAllSeries();

        $this->scrapeAllEpisodes();
    }

    public function scrapeAllSeries()
    {
        $page = 1;

        do {
            $url = $this->provider->url . '/api/courses?page=' . $page;

            info('scraping series: ' . $url);

            $json = json_decode($this->getPage($url));

            $paginatedSeries = $json->data;

            foreach ($paginatedSeries as $aSeries) {
                $dbRecord = Series::updateOrCreate([
                    'name' => $aSeries->title,
                    'provider_id' => $this->provider->id,
                ], [
                    'external_slug' => $aSeries->slug,
                    'description' => $aSeries->description,
                ]);

                $dbRecord->slug = Str::slug($dbRecord->name . ' ' . $dbRecord->id);
                $dbRecord->save();

                foreach ($aSeries->subjects->data as $subject) {
                    $tag = Tag::firstOrCreate([
                        'name' => $subject->title,
                    ], [
                        'slug' => $subject->slug,
                    ]);

                    $tag->slug = Str::slug($tag->name . ' ' . $tag->id);
                    $tag->save();

                    $dbRecord->tags()->sync($tag->id);
                }
            }

            if ($json->meta->pagination->total_pages == $page)
            {
                break;
            }

            $page++;
            sleep(random_int(3, 6));
        } while (true);
    }
    public function scrapeAllEpisodes()
    {
        // @todo get series date from them
        // atm you can only get a general released 1 month ago etc

        Series::withCount('episodes')
            ->whereProviderId($this->provider->id)
            ->get(['id', 'name', 'external_slug'])
            ->each(function ($series){
                $url = $this->provider->url . '/watch/' . $series->external_slug;

                info('scraping episode series: ' . $series->name);

                try {
                    $html = $this->getPage($url);
                } catch (\Exception $e) {
                    info('couldnt parse url:' . $url);
                    return true;
                }

                $html = $this->getEpisodesPart($html);

                preg_match_all('~title:"(.*?)",~', $html, $titles);
                preg_match_all('~,hls:"(.*?)"}~', $html, $url);
                preg_match_all('~{duration:(.*?),~', $html, $durations);

                if ( $series->episodes_count == count($titles[1]) ) {
                    return true; // skip
                }

                // if our episode_count = 0
                // if our episode_count > 0

                $titles = $titles[1]; // 0 1 2 3
                $url = $url[1]; // 1 2
                $durations = $durations[1];

                for($i=$series->episodes_count; $i < count($titles); $i++) {
                    try {
                        $episode = Episode::updateOrCreate([
                                'name' => $titles[$i],
                                'series_id' => $series->id,
                            ], [
                                'duration' => $durations[$i] ?? null,
                                'video' => $url[$i] ?? null,
                            ]
                        );
                    } catch (\Exception $e) {
                        info($e->getMessage(), [
                            'series' => $series->id,
                            'episode' => $titles[$i],
                        ]);
                        info($html);

                        continue;
                    }

                    $episode->slug = Str::slug($episode->name . ' ' . $episode->id);
                    $episode->save();
                }
                sleep(random_int(3, 6));
            });
    }

    private function getPage($path)
    {
        return $this->client
            ->get($path, ['verify' => false])
            ->getBody()
            ->getContents();
    }

    private function getEpisodesPart(string $html)
    {
        preg_match('~parts:\{parts:\[(.*)errors:~', $html, $matches);

        return $matches[1];
    }
}

