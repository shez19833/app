<?php

namespace App\Console\Commands\ScrapingServices;

use App\Models\Episode;
use App\Models\Series;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class LaracastScraper {

    public $client;
    public $provider;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function handle($provider)
    {
        return;
        $this->provider = $provider;

        $seriesPaths = $this->scrapeSeriesUrl();

        foreach ($seriesPaths as $path) {
            $url = $this->provider->url
                . '/series/'
                . $path .
                '/episodes/1';

            dump($url);

            dump('Getting HTML');
            $aSeries = $this->getObjectFromPage($url);

            dump('Getting Series & Episodes');
            $this->scrapeASeriesAndEpisodes($aSeries);

            sleep(random_int(3, 6));
        }
    }

    public function scrapeSeriesUrl()
    {
        // @todo remove this array return..
        return [
            "laravel-6-from-scratch",
            "laravel-explained",
            "whats-new-in-laravel-6",
            "php-bits",
            "mysql-database-design",
            "practical-vue-components",
            "ten-techniques-for-cleaner-code",
            "building-laracasts",
            "whatcha-working-on",
            "learn-vue-2-step-by-step",
            "queue-it-up",
            "modern-css-for-backend-developers",
            "laravel-vue-and-spas",
            "build-a-laravel-app-with-tdd",
            "build-and-configure-a-staging-server",
            "whats-new-in-laravel-5-8",
            "how-do-i",
            "laravel-from-scratch-2018",
            "laravel-nova-mastery",
            "eloquent-relationships",
            "whats-new-in-laravel-5-7",
            "unlocking-badges-workshop",
            "how-to-be-awesome-in-phpstorm",
            "learn-laravel-forge",
            "phpunit-testing-in-laravel",
            "how-to-create-custom-presets",
            "whats-new-in-laravel-5-6",
            "how-to-read-code",
            "code-reflections",
            "how-to-manage-an-open-source-project",
            "learn-laravel-mix",
            "get-real-with-laravel-echo",
            "lets-build-a-forum-with-laravel",
            "visual-studio-code-for-php-developers",
            "testing-vue",
            "css-grids-for-everyone",
            "whats-new-in-laravel-5-5",
            "professional-php-workflow-in-sublime-text",
            "webpack-for-everyone",
            "how-to-accept-payments-with-stripe",
            "whats-new-in-laravel-5-4",
            "setup-a-mac-dev-machine-from-scratch",
            "learn-laravel-and-redis-through-examples",
            "whats-new-in-php-7-1",
            "learn-flexbox-through-examples",
            "whats-new-in-laravel-5-3",
            "hands-on-community-contributions",
            "php-for-beginners",
            "how-to-use-html5-video-and-videojs",
            "discover-symfony-components",
            "laravel-authentication-techniques",
            "es6-cliffsnotes",
            "laravel-spark",
            "whip-monstrous-code-into-shape",
            "charting-and-you",
            "russian-doll-caching-in-laravel",
            "vim-mastery",
            "whats-new-in-laravel-5-2",
            "solid-principles-in-php",
            "design-patterns-in-php",
            "modern-css-workflow",
            "real-time-laravel-with-socket-io",
            "eloquent-techniques",
            "sublime-text-mastery",
            "php7-up-and-running",
            "git-me-some-version-control",
            "whats-new-in-laravel-5-1",
            "intermediate-laravel",
            "do-you-react",
            "intuitive-integration-testing",
            "envoyer",
            "advanced-eloquent",
            "testing-jargon",
            "how-to-build-command-line-apps-in-php",
            "simple-rules-for-simpler-code",
            "object-oriented-bootcamp-in-php",
            "code-katas-in-php",
            "understanding-regular-expressions",
            "the-lifecycle-of-a-new-feature",
        ];

        $html = $this->getPage($this->provider->url . '/series' );
        $parser = new Crawler($html);

        $seriesNodes = $parser->filter(".series-card");

        $series = $seriesNodes->each(function(Crawler $crawler) {
            $slug = str_replace('/series/', '', $crawler->filter('a.tw-block')->attr('href'));
//            $episode_count = (int) $crawler->filter('.card-bottom .card-stats div div.tw-text-xs.tw-font-semibold')->text();

            return $slug;
        });

        return $series;
    }

    private function getObjectFromPage($url)
    {
        info($url);

        $html = $this->getPage($url);

        info($html);

        $matches = [];
        preg_match('~<video-card :video="(.*)" video-length=~', $html, $matches);

        $string = str_replace(
            [
                'Laracasts\Video',
                '&quotquot;'
            ],
            [
                'Laracasts\\\Video',
                '"'
            ],
            html_entity_decode($matches[1])
        );

        return json_decode($string)->series;
    }

    private function scrapeASeriesAndEpisodes($scrapedSeries)
    {
        $DBRecord = Series::withCount('episodes')
            ->whereName($scrapedSeries->title)
            ->first();

        if ( $DBRecord && $DBRecord->updated_at == $scrapedSeries->updated_at ) {
            info("skipping - Series is complete or hasn't changed!");
            return;
        }

        if ( !$DBRecord ) {
            $DBRecord = new Series();
            $DBRecord->fill([
                'cover_image' => $this->provider->url . $scrapedSeries->thumbnail,
                'description' => $scrapedSeries->body,
                'name' => $scrapedSeries->title,
                'provider_id' => $this->provider->id,
                'slug' => $scrapedSeries->slug,
            ]);

            $DBRecord->episodes_count = 0;
        }

        $DBRecord->created_at = $scrapedSeries->created_at;
        $DBRecord->updated_at = $scrapedSeries->updated_at;

        $DBRecord->save(['timestamps' => false]);

        $scrapedEpisodes = array_slice(
            $scrapedSeries->episodes,
            $DBRecord->episodes_count
        );
        
        foreach ($scrapedEpisodes as $scrapedEpisode) {
            $episode = Episode::create([
                'description' => $scrapedEpisode->body,
                'duration' => $scrapedEpisode->length,
                'name' => $scrapedEpisode->title,
                'series_id' => $DBRecord->id,
                'video' => $scrapedEpisode->download,
            ]);

            $episode->slug = $scrapedEpisode->slug . '-' . $scrapedEpisode->id;
            $episode->save(['touch' => true]);
        }
    }

    private function getPage($path)
    {
        return $this->client
            ->get($path, ['verify' => false])
            ->getBody()
            ->getContents();
    }

    private function getEpisodeHTML()
    {
        return <<<'HTML'
<!DOCTYPE html>

<html class="" lang="en">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, user-scalable=no">

<meta property="og:site_name" content="Laracasts">

<meta property="fb:app_id" content="511211375738022">

<meta name="csrf-token" content="2Z1AeqXy7gJOKxtHHVMc90FabPKTg7nLlO1pBAEj">

<link rel="preconnect" href="https://www.google-analytics.com">

<title>Laravel 6 From Scratch: At a Glance</title>

<meta name="description" content="Before we dig into the nuts and bolts of Laravel, let&#039;s first zoom out and discuss what exactly happens when a request co ▶
<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">

<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">

<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">

<link rel="manifest" href="/favicons/site.webmanifest">

<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#3668c9">

<link rel="shortcut icon" href="/favicons/favicon.ico">

<meta name="msapplication-TileColor" content="#ffffff">

<meta name="msapplication-config" content="/favicons/browserconfig.xml">

<meta name="theme-color" content="#ffffff">

<link rel="alternate" type="application/atom+xml" title="Laracasts" href="/feed">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=optional" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Muli:400,800&display=optional" rel="stylesheet">

<link href="/css/app.css?id=a849d463ce37e56bc85d" rel="stylesheet">

<script type="44f9887cfb01aead5e918449-text/javascript">

        window.LARACASTS = {"user":null,"username":null,"signedIn":false,"csrfToken":"2Z1AeqXy7gJOKxtHHVMc90FabPKTg7nLlO1pBAEj","subscribed":null,"teamOwner":nu ▶
    </script>

<meta property="og:title" content="Laracasts">

<meta property="og:description" content="Push your web developments skills to the next level, through expert screencasts on Laravel, Vue, and so much more.">

<meta property="og:type" content="article">

<meta property="og:url" content="https://laracasts.com">

<meta property="og:image" content="https://laracasts.com/images/laracasts-twitter-card.png">

<meta name="twitter:card" content="summary_large_image">

<meta name="twitter:site" content="@laracasts">

<meta name="twitter:title" content="Laracasts">

<meta name="twitter:description" content="Push your web developments skills to the next level, through expert screencasts on Laravel, Vue, and so much more.">

<meta name="twitter:image" content="https://laracasts.com/images/laracasts-twitter-card.png">

<meta property="og:title" content="At a Glance" />

<meta property="og:description" content="Before we dig into the nuts and bolts of Laravel, let&#039;s first zoom out and discuss what exactly happens when a req ▶
<meta property="og:type" content="article" />

<meta property="og:url" content="https://laracasts.com/series/laravel-6-from-scratch/episodes/1" />

<meta property="og:image" content="https://laracasts.com/images/series/2018/pngs/laravel-6-from-scratch.png" />

<meta name="twitter:card" content="summary" />

<meta name="twitter:site" content="@laracasts" />

<meta name="twitter:title" content="At a Glance" />

<meta name="twitter:description" content="Before we dig into the nuts and bolts of Laravel, let&#039;s first zoom out and discuss what exactly happens when a re ▶
<meta name="twitter:image" content="https://laracasts.com/images/series/2018/pngs/laravel-6-from-scratch.png" />

<link rel="stylesheet" href="/highlight/styles/laracasts.css?id=1">

<link rel="prefetch" href="/series/laravel-6-from-scratch/episodes/2">

<link rel="prefetch" href="/series/laravel-6-from-scratch">

</head>

<body class="video guest ">

<script id="flash-template" type="text/template">

    <div class="notification is-primary for-user" dusk="flash-notification">

        <a href="#" class="notification-body inherits-color" target="_blank" rel="noreferrer noopener"></a>

    </div>

</script>

<div id="root" class="page">

<div>

<nav class="section new-nav phone:tw-sticky phone:tw-top-0 phone:tw-z-40 is-minimal tw-bg-laravel tw-py-5 lg:tw-py-3 md:tw-overflow-x-hidden md:tw-overflow-y-hi ▶
<div class="tw-flex tw-justify-between tw-h-full tw-items-center tw-relative">

<div id="header-logo-arrow" class="xl:tw-flex-1 tw-mr-4 xl:tw-mr-0">

<a href="/" @click.right.prevent="visitAssetsWebsite" class="tw-cursor-pointer tw-leading-none tw-block">

<svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">

<g fill="none" fill-rule="evenodd">

<g fill="#FFF">

<path d="M4.608 6.721l3.798 3.799a1.948 1.948 0 0 1-2.754 2.754L1.853 9.476A1.948 1.948 0 0 1 4.608 6.72zM12.631 14.745l8.086 8.085a1.948 1.948 0 1 1-2.755 2.75 ▶
<path d="M2.755 6.22L24.105.836a2.057 2.057 0 0 1 2.483 1.433 1.94 1.94 0 0 1-1.392 2.411l-21.35 5.385a2.057 2.057 0 0 1-2.483-1.434 1.94 1.94 0 0 1 1.392-2.41z ▶
<path d="M17.384 23.604l5.385-21.35A1.94 1.94 0 0 1 25.179.86a2.057 2.057 0 0 1 1.434 2.483l-5.384 21.35a1.94 1.94 0 0 1-2.411 1.392 2.057 2.057 0 0 1-1.434-2.4 ▶
</g>

<path class="tw-fill-current" d="M16.541 13.778l-7.63 7.631a2.015 2.015 0 1 1-2.85-2.849l7.631-7.63a2.015 2.015 0 1 1 2.85 2.848zM5.111 25.208l-1.108 1.109a2.01 ▶
</g>

</svg>

</a>

</div>

<div class="md:tw-hidden tw-flex tw-items-start">

<search-button id="mobile-search" class="tw-mr-4 ">

<button id="search-trigger" class="tw-leading-none" title="Tip: press / or s anywhere to instantly activate me.">

<svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 15 15">

<g fill="none" fill-rule="evenodd">

<path d="M-2-2h20v20H-2z"></path>

<path fill="#FFF" d="M10.443 9.232h-.638l-.226-.218A5.223 5.223 0 0 0 10.846 5.6 5.247 5.247 0 1 0 5.6 10.846c1.3 0 2.494-.476 3.414-1.267l.218.226v.638l4.036 4 ▶
</g>

</svg>

</button>

</search-button>

<nav-modal></nav-modal>

</div>

<div class="tw-hidden md:tw-flex md:tw-justify-center md:tw-mx-auto tw-text-sm xl:tw-pt-2">

<series-mega-menu classes="tw-text-white is-active " nav-mode="one-column">

<a href="/series" class="tw-text-white is-active tw-block md:tw-px-4 xl:tw-px-10 tw-uppercase tw-text-transparent-50 tw-font-semibold hover:tw-text-white">

Series

</a>

</series-mega-menu>

<a href="/discuss" class=" tw-block md:tw-px-4 xl:tw-px-10 tw-uppercase tw-text-transparent-50 tw-font-semibold hover:tw-text-white">

Discussions

</a>

<a href="/podcast" class=" tw-relative tw-block md:tw-px-4 xl:tw-px-10 tw-uppercase tw-text-transparent-50 tw-font-semibold hover:tw-text-white">

Podcast

</a>

<a href="/search" class=" tw-block md:tw-px-4 xl:tw-px-10 tw-uppercase tw-text-transparent-50 tw-font-semibold hover:tw-text-white">

Search

</a>

</div>

<div class="tw-hidden md:tw-block tw-relative tw-flex-1">

<div class="tw-flex tw-items-center tw-justify-end">

<search-button id="search-trigger" class="tw-leading-none tw-bg-transparent-10 hover:tw-bg-transparent-25 tw-p-3 tw-rounded-xl">

<button id="search-trigger" title="Tip: press / or s anywhere to instantly activate me.">

<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">

<g fill="none" fill-rule="evenodd">

<path d="M-2-2h20v20H-2z"></path>

<path fill="#FFF" d="M10.443 9.232h-.638l-.226-.218A5.223 5.223 0 0 0 10.846 5.6 5.247 5.247 0 1 0 5.6 10.846c1.3 0 2.494-.476 3.414-1.267l.218.226v.638l4.036 4 ▶
</g>

</svg>

</button>

</search-button>

<a href="/login" class="tw-text-white hover:tw-text-white link tw-font-bold tw-uppercase tw-mx-6 tw-text-xs" @click.prevent="dispatch('Login')">

Sign In

</a>

<a class="tw-text-white hover:tw-bg-white hover:tw-text-blue tw-font-bold tw-uppercase tw-border tw-rounded-full tw-text-xs tw-px-3 tw-py-2 tw-leading-tight" hr ▶
Get Started

</a>

</div>

</div>

</div>

</nav>

<nav class="section tw-py-2 tw-bg-blue-darkest tw-hidden lg:tw-block">

<p class="tw-flex tw-items-center tw-text-transparent-50 tw-text-xs tw-font-semibold">

<a href="/series?curated" class="inherits-color hover:tw-text-white md:hover:tw-bg-black-transparent-15 tw-rounded-lg tw-px-2 tw--mx-2 tw-py-1">

Browse

</a>

<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">

<g fill="none" fill-rule="evenodd" opacity=".5">

<path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M1 21V.84h20.16V21z"></path>

<path fill="#FFF" d="M8.146 14.776l3.847-3.856-3.847-3.856L9.33 5.88l5.04 5.04-5.04 5.04z"></path>

</g>

</svg>

<a href="/skills/laravel" class="inherits-color hover:tw-text-white md:hover:tw-bg-black-transparent-15 tw-rounded-lg tw-px-2 tw--mx-2 tw-py-1">

Laravel

</a>

<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">

<g fill="none" fill-rule="evenodd" opacity=".5">

<path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M1 21V.84h20.16V21z"></path>

<path fill="#FFF" d="M8.146 14.776l3.847-3.856-3.847-3.856L9.33 5.88l5.04 5.04-5.04 5.04z"></path>

</g>

</svg>

<a href="/series/laravel-6-from-scratch" class="inherits-color hover:tw-text-white md:hover:tw-bg-black-transparent-15 tw-rounded-lg tw-px-2 tw--mx-2 tw-py-1">\ ▶
Laravel 6 From Scratch

</a>

<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">

<g fill="none" fill-rule="evenodd" opacity=".5">

<path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M1 21V.84h20.16V21z"></path>

<path fill="#FFF" d="M8.146 14.776l3.847-3.856-3.847-3.856L9.33 5.88l5.04 5.04-5.04 5.04z"></path>

</g>

</svg>

<span class="tw-text-white">Episode 1 - At a Glance</span>

</p>

</nav>

<portal-target name="nav-after"></portal-target>

<lesson-view type="episode" :id="1567 " :is-complete="false" :is-viewable="true" inline-template>

<div>

<section class="tw-bg-deep-black lg:tw-bg-grey-light tw-p-0" v-cloak>

<div class="video-main tw-relative tw-flex tw-justify-center lg:tw-py-3 lg:tw-px-2 tw-mx-auto" :class="expanded || ! isViewable ? 'tw-bg-deep-black' : '' ">

<div v-show="! expanded && isViewable" class="js-video-episode-list tw-rounded-lg tw-overflow-auto tw-mr-4 tw-hidden lg:tw-block tw-z-10" style="width: 331px">\ ▶
<div class="tw-mr-4" style="width: 331px;" :style="expanded || ! isViewable ? 'max-height: 1000px' : 'height: 0'" v-cloak>

<a href="https://www.understand.io/?utm_source=laracasts&utm_medium=banner&utm_campaign=laracasts19" target="_blank" class="tw-mt-1 tw-mx-auto tw-block tw-curso ▶
<div class="tw-flex tw-items-center tw-bg-white tw-py-4 tw-px-5 tw-border tw-rounded" :class="expanded ? 'tw-border-dotted tw-border-grey-light' : 'tw-border-da ▶
<img src="/images/call-to-action/understand-io.png?v=2" alt="Understand.io logo" class="tw-mr-4" width="48" height="48">

<div class="tw-flex-1 tw-text-black">

<h5 class="lg:tw-text-sm tw-font-semibold tw-mb-2 hover:tw-text-blue">Laravel Error Tracking <span class="tw-text-3xs tw-text-grey-dark tw-relative tw-pl-1" sty ▶
<p class="tw-text-xs lg:tw-text-2xs tw-mb-2">

<strong class="hover:tw-text-blue">Understand.io</strong> is used by hundreds of Laravel developers to find and fix errors.

</p>

<p class="tw-text-xs lg:tw-text-2xs">

<strong class="hover:tw-text-blue">Sign up for a free 14-day trial now.</strong>

</p>

</div>

</div>

</a>

<aside class="tw-hidden lg:tw-block tw-bg-white tw-p-2 tw-rounded-lg tw-overflow-y-auto" :class="expanded ? 'tw-border tw-border-dotted tw-border-grey-light tw- ▶
<header class="sidebar-series-card tw-transition-all hover:tw-bg-laravel tw-rounded-lg tw-cursor-pointer">

<a href="/series/laravel-6-from-scratch" class="tw-block tw-flex tw-items-center tw-py-3 tw-px-4">

<img src="/images/series/2018/laravel-6-from-scratch.svg" alt="Laravel 6 From Scratch" width="48" height="48" class="tw-mr-3 tw-flex-shrink-0">

<div>

<h2 class="sidebar-series-card-title tw-font-bold tw-mb-2 tw-leading-none tw-text-black">Laravel 6 From Scratch</h2>

<div class="tw-flex tw-items-center tw-text-3xs">

<div class="tw-flex tw-items-center tw-leading-none">

<strong class="sidebar-series-card-series-difficulty tw-mr-2 group-hover:tw-text-white">SERIES</strong>

<div class="tw-mr-4 difficulty-meter tw-flex tw-flex-row-reverse is-intermediate">

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px; margin-left: 3px;"></span>

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px; margin-left: 3px;"></span>

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px;"></span>

</div>

</div>

<div class="tw-inline-flex tw-items-center tw-mr-2">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="11" viewBox="0 0 9 11" class="sidebar-series-card-icon tw-text-grey-dark tw-mr-1">

<path class="tw-fill-current" fill-rule="nonzero" d="M2.642 4.394v3.768a.393.393 0 0 1-.252.372c-.214.079-.472.12-.747.12-.79 0-1.641-.338-1.641-1.081V2.751c-.0 ▶
</svg>

<strong class="sidebar-series-card-stat tw-text-grey-dark">29 Lessons</strong>

</div>

<div class="tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8" class="sidebar-series-card-icon tw-text-grey-dark tw-relative" style="margin-righ ▶
<path class="tw-fill-current" fill-rule="evenodd" d="M4 0C1.8 0 0 1.8 0 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm1.826 5.538L3.692 4.254V1.846h.616v2.087l1.846 1.084-.3 ▶
</svg>

<strong class="sidebar-series-card-stat tw-text-grey-dark">2:21:24 hrs</strong>

</div>

</div>

</div>

</a>

</header>

<hr style="margin-top: 0; margin-bottom: 8px;" class="tw-border-grey-lighter">

<ol class="episode-list is-condensed" v-cloak>

<episode-list-chapter :chapter="1" :episode-count="4" inline-template>

<li id="chapter-1">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 1</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Prerequisites

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1567" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="01" :is-scheduled="false" skill="laravel" :id="1567" :is-current="true" :completed="false" progress- ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/1" class="tw-text-blue hover:tw-text-blue tw-font-bold" style="overflow:hidden; width: 225px; text-overflow: el ▶
At a Glance

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 1</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1569" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="02" :is-scheduled="false" skill="laravel" :id="1569" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

 <h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/2" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Install PHP, MySQL and Composer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 2</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:33

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1570" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="03" :is-scheduled="false" skill="laravel" :id="1570" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/3" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
The Laravel Installer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 3</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:02

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1571" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="04" :is-scheduled="false" skill="laravel" :id="1571" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/4" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Laravel Valet Setup

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 4</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:18

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="2" :episode-count="4" inline-template>

<li id="chapter-2">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 2</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Routing

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

 <path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1573" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="05" :is-scheduled="false" skill="laravel" :id="1573" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/5" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Basic Routing and Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 5</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:41

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1574" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="06" :is-scheduled="false" skill="laravel" :id="1574" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/6" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Pass Request Data to Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 6</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1575" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="07" :is-scheduled="false" skill="laravel" :id="1575" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/7" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Route Wildcards

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 7</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:42

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1576" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="08" :is-scheduled="false" skill="laravel" :id="1576" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/8" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Routing to Controllers

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 8</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:01

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="3" :episode-count="5" inline-template>

<li id="chapter-3">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 3</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Database Access

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

 <path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1578" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="09" :is-scheduled="false" skill="laravel" :id="1578" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/9" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Setup a Database Connection

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 9</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:13

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1579" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="10" :is-scheduled="false" skill="laravel" :id="1579" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/10" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Hello Eloquent

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 10</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1580" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="11" :is-scheduled="false" skill="laravel" :id="1580" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/11" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Migrations 101

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 11</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:23

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1581" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="12" :is-scheduled="false" skill="laravel" :id="1581" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/12" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Generate Multiple Files in a Single Command

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 12</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

1:26

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1582" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="13" :is-scheduled="false" skill="laravel" :id="1582" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/13" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
 Business Logic

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 13</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:36

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="4" :episode-count="7" inline-template>

<li id="chapter-4">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 4</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Views

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1583" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="14" :is-scheduled="false" skill="laravel" :id="1583" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/14" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Layout Pages

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 14</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1584" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="15" :is-scheduled="false" skill="laravel" :id="1584" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/15" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Integrate a Site Template

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 15</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:27

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1585" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="16" :is-scheduled="false" skill="laravel" :id="1585" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/16" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Set an Active Menu Link

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 16</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:15

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1586" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="17" :is-scheduled="false" skill="laravel" :id="1586" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/17" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Asset Compilation with Laravel Mix and webpack

</a>

 </h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 17</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:39

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1588" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="18" :is-scheduled="false" skill="laravel" :id="1588" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/18" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Render Dynamic Data

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 18</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:19

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1589" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="19" :is-scheduled="false" skill="laravel" :id="1589" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/19" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Render Dynamic Data: Part 2

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 19</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:05

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1590" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="20" :is-scheduled="false" skill="laravel" :id="1590" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/20" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Homework Solutions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 20</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

 <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="5" :episode-count="5" inline-template>

<li id="chapter-5">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 5</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Forms

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1592" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="21" :is-scheduled="false" skill="laravel" :id="1592" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/21" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
The Seven Restful Controller Actions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 21</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:03

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1593" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="22" :is-scheduled="false" skill="laravel" :id="1593" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/22" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Restful Routing

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 22</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:37

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1594" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="23" :is-scheduled="false" skill="laravel" :id="1594" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/23" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Handling

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 23</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:55

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1596" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="24" :is-scheduled="false" skill="laravel" :id="1596" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/24" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Forms That Submit PUT Requests

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 24</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:35

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1598" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="25" :is-scheduled="false" skill="laravel" :id="1598" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/25" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Validation Essentials

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 25</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

8:53

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="6" :episode-count="3" inline-template>

<li id="chapter-6">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 6</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Controller Techniques

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

 <template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1599" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="26" :is-scheduled="false" skill="laravel" :id="1599" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/26" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Leverage Route Model Binding

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 26</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:56

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1600" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="27" :is-scheduled="false" skill="laravel" :id="1600" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/27" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Reduce Duplication

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 27</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1601" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="28" :is-scheduled="false" skill="laravel" :id="1601" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/28" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Consider Named Routes

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 28</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:58

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

</ol> </aside>

</div>

</div>

<video-player id="laracasts-video-wrap" lesson="1567" vimeo-id="358821345" :expanded="expanded" class="tw-flex-1 tw-overflow-hidden lg:tw-rounded-lg" :class="ex ▶
<div @mouseover="isHovered = true" @mouseout="isHovered = false">

<div id="laracasts-video" style="-webkit-mask-image: -webkit-radial-gradient(white, black);" class="lg:tw-rounded-lg tw-bg-deep-black"></div>

<prev-next-buttons :expanded="expanded" :show="isHovered || expanded" class="mobile:tw-hidden" :class="isHovered ? '' : 'tw-visibility-hidden'" inline-template> ▶
<div class="prev-next-buttons" style="transition: opacity .3s">

<div class="tw-flex tw-absolute" style="top:42px; height: 36px;" :style="expanded ? { left: '0' } : { left: '' }">

<div class="tw-cursor-pointer tw-flex tw-justify-center tw-mr-2 tw-bg-blue-darker hover:tw-bg-blue-darkest" style="width: 46px" @mouseover="showTitle = true" @m ▶
<button @click="dispatchToggle" class="tw-w-full tw-h-full tw-flex tw-justify-center tw-items-center tw-px-1">

<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" :style="expanded ? { transform: 'rotate(180deg)' } : {}">

<g fill="none" fill-rule="evenodd">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-4-4h24v24H-4z"></path>

<path fill="#FFF" d="M16 7H3.83l5.59-5.59L8 0 0 8l8 8 1.41-1.41L3.83 9H16z"></path>

</g>

</svg>

</button>

</div>

<div style="transition: opacity .3s" class="tw-cursor-pointer tw-flex tw-items-center tw-bg-blue-darker hover:tw-bg-blue-darkest tw-px-5" :class="showTitle ? 't ▶
<button class="tw-text-white hover:tw-text-white" @click="dispatchToggle">

{{ expanded ? 'Expand' : 'Close' }} Episode List

</button>

</div>

</div>

<div>

<div id="laracasts-video-next-button" class="tw-flex tw-absolute" :style="expanded ? { right: '0' } : { right: '0' }" style="top: calc(50% - 32px); height: 64px ▶
<div style="transition: opacity .3s" class="tw-flex tw-mr-2 tw-bg-blue-darker hover:tw-bg-blue-darkest" :class="showTitle ? 'tw-opacity-100' : 'tw-opacity-0 tw- ▶
<a href="/series/laravel-6-from-scratch/episodes/2?autoplay=true" class="tw-flex tw-items-center tw-px-5 tw-text-white hover:tw-text-white">

02. Install PHP, MySQL and Composer

</a>

</div>

<div class="tw-cursor-pointer tw-flex tw-bg-blue-darker hover:tw-bg-blue-darkest" @mouseover="showTitle = true" @mouseout="showTitle = false">

<a href="/series/laravel-6-from-scratch/episodes/2?autoplay=true" class="tw-flex tw-items-center tw-px-1">

<svg xmlns="http://www.w3.org/2000/svg" width="37" height="38" viewBox="0 0 37 38">

<g fill="none" fill-rule="evenodd">

<path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M1 37V1h36v36z"></path>

<path fill="#FFF" d="M13.76 25.885L20.63 19l-6.87-6.885L15.875 10l9 9-9 9z"></path>

</g>

</svg>

</a>

</div>

</div>

</div>

</div>

</prev-next-buttons>

</div>

</video-player>

</div>

</section>

<ad></ad>

<div class="video-details section" v-cloak>

<div class="tw-mx-auto">

<div>

<div style="max-width: 886px" class="tw-flex tw-mb-8 tw-mx-auto tw-flex-wrap lg:tw-flex-no-wrap">

<video-card :video="{&quot;id&quot;:1571,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Laravel Valet Setup&quot;,&quot;body&quot;:&quot;&lt;p&gt;If you&#039;re a Mac user, rather than running &lt;code&gt;php artisan serve&lt;\/code&gt;, you might instead choose to install &lt;a href=\&quot;https:\/\/laravel.com\/docs\/6.0\/valet\&quot;&gt;Laravel Valet&lt;\/a&gt;. Valet is a blazing fast development environment for Laravel that&#039;s a cinch to setup.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;laravel-valet-setup&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:6742,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359342471.hd.mp4?s=b6f492a5eb67a6623665737da3c5d30973e49fae&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359342471.hd.mp4?s=b6f492a5eb67a6623665737da3c5d30973e49fae&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:198,&quot;series_id&quot;:102,&quot;position&quot;:4,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-11 12:53:20&quot;,&quot;updated_at&quot;:&quot;2019-09-11 12:53:20&quot;,&quot;published_at&quot;:&quot;2019-09-13 09:00:00&quot;,&quot;chapter&quot;:1,&quot;indices&quot;:[{&quot;id&quot;:3057,&quot;name&quot;:&quot;valet&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1571},{&quot;id&quot;:3058,&quot;name&quot;:&quot;server&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1571}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;},&quot;series&quot;:{&quot;id&quot;:102,&quot;skill_id&quot;:1,&quot;title&quot;:&quot;Laravel 6 From Scratch&quot;,&quot;custom_url&quot;:&quot;laravelfromscratch.com&quot;,&quot;body&quot;:&quot;&lt;p&gt;In this series, step by step, I&#039;ll show you how to build web applications with Laravel 6. We&#039;ll start with the basics and incrementally dig deeper and deeper, as we review real-life examples. Once complete, you should have all the tools you need. Let&#039;s get to work!&lt;\/p&gt;&quot;,&quot;difficulty&quot;:&quot;2&quot;,&quot;theme&quot;:null,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-6-from-scratch.svg&quot;,&quot;slug&quot;:&quot;laravel-6-from-scratch&quot;,&quot;replaced_by_id&quot;:null,&quot;created_at&quot;:&quot;2019-09-09 14:42:36&quot;,&quot;updated_at&quot;:&quot;2019-10-09 19:38:37&quot;,&quot;episode_count&quot;:29,&quot;length&quot;:8484,&quot;complete&quot;:0,&quot;learning_points&quot;:&quot;&quot;,&quot;archived&quot;:0,&quot;chapters&quot;:[&quot;Prerequisites&quot;,&quot;Routing&quot;,&quot;Database Access&quot;,&quot;Views&quot;,&quot;Forms&quot;,&quot;Controller Techniques&quot;],&quot;difficultyLevel&quot;:&quot;Intermediate&quot;,&quot;path&quot;:&quot;\/series\/laravel-6-from-scratch&quot;,&quot;lengthForHumans&quot;:&quot;2:21:24 hrs&quot;,&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;},&quot;episodes&quot;:[{&quot;id&quot;:1567,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;At a Glance&quot;,&quot;body&quot;:&quot;&lt;p&gt;Before we dig into the nuts and bolts of Laravel, let&#039;s first zoom out and discuss what exactly happens when a request comes in.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;at-a-glance&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:15277,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/358821345.hd.mp4?s=19cc3cb047f44b959cb8ca3bcc276b3910379cfe&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/358821345.hd.mp4?s=19cc3cb047f44b959cb8ca3bcc276b3910379cfe&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:160,&quot;series_id&quot;:102,&quot;position&quot;:1,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-09 12:36:38&quot;,&quot;updated_at&quot;:&quot;2019-09-09 12:36:38&quot;,&quot;published_at&quot;:&quot;2019-09-09 12:34:26&quot;,&quot;chapter&quot;:1,&quot;indices&quot;:[{&quot;id&quot;:3053,&quot;name&quot;:&quot;animation&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1567}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1569,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Install PHP, MySQL and Composer&quot;,&quot;body&quot;:&quot;&lt;p&gt;Before we get started, you must first ensure that up-to-date versions of both PHP and MySQL are installed and available on your machine. In this episode, we&#039;ll review how to go about this. Once complete, we can then install &lt;a href=\&quot;http:\/\/getcomposer.org\&quot;&gt;Composer&lt;\/a&gt;.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;install-php,-mysql-and-composer&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:10027,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359315312.hd.mp4?s=6a7b2a92e0a773f677b571c4506cb718b9e3438b&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359315312.hd.mp4?s=6a7b2a92e0a773f677b571c4506cb718b9e3438b&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:213,&quot;series_id&quot;:102,&quot;position&quot;:2,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-11 11:48:24&quot;,&quot;updated_at&quot;:&quot;2019-09-11 11:48:24&quot;,&quot;published_at&quot;:&quot;2019-09-11 11:45:47&quot;,&quot;chapter&quot;:1,&quot;indices&quot;:[{&quot;id&quot;:3054,&quot;name&quot;:&quot;composer&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1569},{&quot;id&quot;:3055,&quot;name&quot;:&quot;brew&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1569}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1570,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;The Laravel Installer&quot;,&quot;body&quot;:&quot;&lt;p&gt;Now that we have Composer setup, we can pull in the Laravel installer and make it accessible globally on our machine. This allows you to run a single command to build a fresh Laravel installation: &lt;code&gt;laravel new app&lt;\/code&gt;.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;the-laravel-installer&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:8102,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359338337.hd.mp4?s=ca9ac62ee6bf726df51512dfedc044eebc836481&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359338337.hd.mp4?s=ca9ac62ee6bf726df51512dfedc044eebc836481&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:182,&quot;series_id&quot;:102,&quot;position&quot;:3,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-11 12:33:04&quot;,&quot;updated_at&quot;:&quot;2019-09-11 12:33:04&quot;,&quot;published_at&quot;:&quot;2019-09-12 09:00:00&quot;,&quot;chapter&quot;:1,&quot;indices&quot;:[{&quot;id&quot;:3056,&quot;name&quot;:&quot;composer&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1570}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;},&quot;series&quot;:{&quot;id&quot;:102,&quot;skill_id&quot;:1,&quot;title&quot;:&quot;Laravel 6 From Scratch&quot;,&quot;custom_url&quot;:&quot;laravelfromscratch.com&quot;,&quot;body&quot;:&quot;&lt;p&gt;In this series, step by step, I&#039;ll show you how to build web applications with Laravel 6. We&#039;ll start with the basics and incrementally dig deeper and deeper, as we review real-life examples. Once complete, you should have all the tools you need. Let&#039;s get to work!&lt;\/p&gt;&quot;,&quot;difficulty&quot;:&quot;2&quot;,&quot;theme&quot;:null,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-6-from-scratch.svg&quot;,&quot;slug&quot;:&quot;laravel-6-from-scratch&quot;,&quot;replaced_by_id&quot;:null,&quot;created_at&quot;:&quot;2019-09-09 14:42:36&quot;,&quot;updated_at&quot;:&quot;2019-10-09 19:38:37&quot;,&quot;episode_count&quot;:29,&quot;length&quot;:8484,&quot;complete&quot;:0,&quot;learning_points&quot;:&quot;&quot;,&quot;archived&quot;:0,&quot;chapters&quot;:[&quot;Prerequisites&quot;,&quot;Routing&quot;,&quot;Database Access&quot;,&quot;Views&quot;,&quot;Forms&quot;,&quot;Controller Techniques&quot;],&quot;difficultyLevel&quot;:&quot;Intermediate&quot;,&quot;path&quot;:&quot;\/series\/laravel-6-from-scratch&quot;,&quot;lengthForHumans&quot;:&quot;2:21:24 hrs&quot;,&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}}},{&quot;id&quot;:1571,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Laravel Valet Setup&quot;,&quot;body&quot;:&quot;&lt;p&gt;If you&#039;re a Mac user, rather than running &lt;code&gt;php artisan serve&lt;\/code&gt;, you might instead choose to install &lt;a href=\&quot;https:\/\/laravel.com\/docs\/6.0\/valet\&quot;&gt;Laravel Valet&lt;\/a&gt;. Valet is a blazing fast development environment for Laravel that&#039;s a cinch to setup.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;laravel-valet-setup&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:6917,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359342471.hd.mp4?s=b6f492a5eb67a6623665737da3c5d30973e49fae&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359342471.hd.mp4?s=b6f492a5eb67a6623665737da3c5d30973e49fae&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:198,&quot;series_id&quot;:102,&quot;position&quot;:4,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-11 12:53:20&quot;,&quot;updated_at&quot;:&quot;2019-09-11 12:53:20&quot;,&quot;published_at&quot;:&quot;2019-09-13 09:00:00&quot;,&quot;chapter&quot;:1,&quot;indices&quot;:[{&quot;id&quot;:3057,&quot;name&quot;:&quot;valet&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1571},{&quot;id&quot;:3058,&quot;name&quot;:&quot;server&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1571}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1573,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Basic Routing and Views&quot;,&quot;body&quot;:&quot;&lt;p&gt;When I learn a new framework, the first thing I do is figure out how the framework&#039;s default splash page is loaded. Let&#039;s work through it together. Our first stop is &lt;code&gt;routes\/web.php&lt;\/code&gt;.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;basic-routing-and-views&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:8106,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359544363.hd.mp4?s=4464523b0acf9c78e6632c588935002e7c33c800&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359544363.hd.mp4?s=4464523b0acf9c78e6632c588935002e7c33c800&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:221,&quot;series_id&quot;:102,&quot;position&quot;:5,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-12 10:09:18&quot;,&quot;updated_at&quot;:&quot;2019-09-12 10:09:18&quot;,&quot;published_at&quot;:&quot;2019-09-14 09:00:00&quot;,&quot;chapter&quot;:2,&quot;indices&quot;:[{&quot;id&quot;:3059,&quot;name&quot;:&quot;route&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1573},{&quot;id&quot;:3060,&quot;name&quot;:&quot;view&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1573}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;},&quot;series&quot;:{&quot;id&quot;:102,&quot;skill_id&quot;:1,&quot;title&quot;:&quot;Laravel 6 From Scratch&quot;,&quot;custom_url&quot;:&quot;laravelfromscratch.com&quot;,&quot;body&quot;:&quot;&lt;p&gt;In this series, step by step, I&#039;ll show you how to build web applications with Laravel 6. We&#039;ll start with the basics and incrementally dig deeper and deeper, as we review real-life examples. Once complete, you should have all the tools you need. Let&#039;s get to work!&lt;\/p&gt;&quot;,&quot;difficulty&quot;:&quot;2&quot;,&quot;theme&quot;:null,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-6-from-scratch.svg&quot;,&quot;slug&quot;:&quot;laravel-6-from-scratch&quot;,&quot;replaced_by_id&quot;:null,&quot;created_at&quot;:&quot;2019-09-09 14:42:36&quot;,&quot;updated_at&quot;:&quot;2019-10-09 19:38:37&quot;,&quot;episode_count&quot;:29,&quot;length&quot;:8484,&quot;complete&quot;:0,&quot;learning_points&quot;:&quot;&quot;,&quot;archived&quot;:0,&quot;chapters&quot;:[&quot;Prerequisites&quot;,&quot;Routing&quot;,&quot;Database Access&quot;,&quot;Views&quot;,&quot;Forms&quot;,&quot;Controller Techniques&quot;],&quot;difficultyLevel&quot;:&quot;Intermediate&quot;,&quot;path&quot;:&quot;\/series\/laravel-6-from-scratch&quot;,&quot;lengthForHumans&quot;:&quot;2:21:24 hrs&quot;,&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}}},{&quot;id&quot;:1574,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Pass Request Data to Views&quot;,&quot;body&quot;:&quot;&lt;p&gt;The &lt;code&gt;request()&lt;\/code&gt; helper function can be used to fetch data from any &lt;code&gt;GET&lt;\/code&gt; or &lt;code&gt;POST&lt;\/code&gt; request. In this episode, we&#039;ll learn how to fetch data from the query-string, pass it to a view, and then encode it to protected against potential XSS attacks.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;pass-request-data-to-views&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:6511,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359605051.hd.mp4?s=0ccdd7189b2468eb2e83c4b03e89531983d61ab9&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359605051.hd.mp4?s=0ccdd7189b2468eb2e83c4b03e89531983d61ab9&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:251,&quot;series_id&quot;:102,&quot;position&quot;:6,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-12 14:25:31&quot;,&quot;updated_at&quot;:&quot;2019-09-12 14:25:31&quot;,&quot;published_at&quot;:&quot;2019-09-15 09:00:00&quot;,&quot;chapter&quot;:2,&quot;indices&quot;:[{&quot;id&quot;:3061,&quot;name&quot;:&quot;request&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1574},{&quot;id&quot;:3062,&quot;name&quot;:&quot;view&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1574}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1575,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Route Wildcards&quot;,&quot;body&quot;:&quot;&lt;p&gt;Often, you&#039;ll need to construct a route that accepts a wildcard value. For example, when viewing a specific post, part of the URI will need to be unique. In these cases, we can reach for a route wildcard.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;route-wildcards&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:5734,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359797630.hd.mp4?s=5b703a6ddf2d9e8d3c7e1fa1f69a413c7450b83d&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359797630.hd.mp4?s=5b703a6ddf2d9e8d3c7e1fa1f69a413c7450b83d&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:222,&quot;series_id&quot;:102,&quot;position&quot;:7,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-13 11:00:50&quot;,&quot;updated_at&quot;:&quot;2019-09-13 11:00:50&quot;,&quot;published_at&quot;:&quot;2019-09-16 09:00:00&quot;,&quot;chapter&quot;:2,&quot;indices&quot;:[{&quot;id&quot;:3063,&quot;name&quot;:&quot;route&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1575}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1576,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Routing to Controllers&quot;,&quot;body&quot;:&quot;&lt;p&gt;It&#039;s neat that we can provide a closure to handle any route&#039;s logic, however, you might find that for more sizable projects, you&#039;ll almost always reach for a dedicated controller instead. Let&#039;s learn how in this lesson.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;routing-to-controllers&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:5160,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/359800679.hd.mp4?s=2e9ddd504375244d3d3e634179b2d5ad492652e3&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/359800679.hd.mp4?s=2e9ddd504375244d3d3e634179b2d5ad492652e3&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:181,&quot;series_id&quot;:102,&quot;position&quot;:8,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-13 11:03:17&quot;,&quot;updated_at&quot;:&quot;2019-09-13 11:03:17&quot;,&quot;published_at&quot;:&quot;2019-09-17 09:00:00&quot;,&quot;chapter&quot;:2,&quot;indices&quot;:[{&quot;id&quot;:3064,&quot;name&quot;:&quot;route&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1576},{&quot;id&quot;:3065,&quot;name&quot;:&quot;controller&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1576}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1578,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Setup a Database Connection&quot;,&quot;body&quot;:&quot;&lt;p&gt;So far, we&#039;ve been using a simple array as our data store. This isn&#039;t very realistic, so let&#039;s learn how to set up a database connection. In this episode, we&#039;ll discuss environment variables, configuration files, and the query builder.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;setup-a-database-connection&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:5113,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/360562634.hd.mp4?s=7703dbeb783dd33cc09767846672349f00485f0b&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/360562634.hd.mp4?s=7703dbeb783dd33cc09767846672349f00485f0b&amp;profile_id=175&quot;,&quot;difficulty&quot;:1,&quot;experience&quot;:100,&quot;length&quot;:373,&quot;series_id&quot;:102,&quot;position&quot;:9,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-17 11:03:39&quot;,&quot;updated_at&quot;:&quot;2019-09-17 11:03:39&quot;,&quot;published_at&quot;:&quot;2019-09-18 09:00:00&quot;,&quot;chapter&quot;:3,&quot;indices&quot;:[{&quot;id&quot;:3069,&quot;name&quot;:&quot;database&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1578},{&quot;id&quot;:3070,&quot;name&quot;:&quot;mysql&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1578}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1579,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Hello Eloquent&quot;,&quot;body&quot;:&quot;&lt;p&gt;In the previous episode, we used the query builder to fetch the relevant post from the database. However, there&#039;s a second option we should consider: Eloquent. Not only does an Eloquent class provide the same clean API for querying your database, but it&#039;s also the perfect place to store any appropriate business logic. &lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;hello-eloquent&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:4306,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/360599980.hd.mp4?s=27af5b8ac0c7659fff9924ce8c64a966bf75c30f&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/360599980.hd.mp4?s=27af5b8ac0c7659fff9924ce8c64a966bf75c30f&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:225,&quot;series_id&quot;:102,&quot;position&quot;:10,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-17 13:32:30&quot;,&quot;updated_at&quot;:&quot;2019-09-17 13:32:30&quot;,&quot;published_at&quot;:&quot;2019-09-19 09:00:00&quot;,&quot;chapter&quot;:3,&quot;indices&quot;:[{&quot;id&quot;:3071,&quot;name&quot;:&quot;eloquent&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1579}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1580,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Migrations 101&quot;,&quot;body&quot;:&quot;&lt;p&gt;In a previous episode, we manually created a database table; however, this doesn&#039;t reflect the typical workflow you&#039;ll follow in your day-to-day coding. Instead, you&#039;ll more typically reach for migration classes. In this episode, we&#039;ll discuss what they are and why they&#039;re useful.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;migrations-101&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3681,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/360826924.hd.mp4?s=9002050b7619ba84af6d1bc0947153fa46d2dcec&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/360826924.hd.mp4?s=9002050b7619ba84af6d1bc0947153fa46d2dcec&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:323,&quot;series_id&quot;:102,&quot;position&quot;:11,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-18 11:29:36&quot;,&quot;updated_at&quot;:&quot;2019-09-18 11:29:36&quot;,&quot;published_at&quot;:&quot;2019-09-20 09:00:00&quot;,&quot;chapter&quot;:3,&quot;indices&quot;:[{&quot;id&quot;:3072,&quot;name&quot;:&quot;migration&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1580}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1581,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Generate Multiple Files in a Single Command&quot;,&quot;body&quot;:&quot;&lt;p&gt;It can quickly become tedious to generate all the various files you need. \&quot;Let&#039;s make a model, and now a migration, and now a controller.\&quot; Instead, we can generate everything we need in a single command. I&#039;ll show you how in this episode.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;generate-multiple-files-in-a-single-command&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3798,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/361331854.hd.mp4?s=0eb55e381b37df88ac8aefaec5837dd1f6bef290&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/361331854.hd.mp4?s=0eb55e381b37df88ac8aefaec5837dd1f6bef290&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:86,&quot;series_id&quot;:102,&quot;position&quot;:12,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-20 11:42:38&quot;,&quot;updated_at&quot;:&quot;2019-09-20 11:42:38&quot;,&quot;published_at&quot;:&quot;2019-09-21 09:00:00&quot;,&quot;chapter&quot;:3,&quot;indices&quot;:[{&quot;id&quot;:3073,&quot;name&quot;:&quot;generator&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1581}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1582,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Business Logic&quot;,&quot;body&quot;:&quot;&lt;p&gt;When possible, the code you write should reflect the manner in which you speak about the product in real life. For example, if you run a school and need a way for students to complete assignments, let&#039;s work those terms into the code. Perhaps you should have an &lt;code&gt;Assignment&lt;\/code&gt; model that includes a &lt;code&gt;complete()&lt;\/code&gt; method.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;business-logic&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:4505,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/361333424.hd.mp4?s=95995f98c6c1da90716fc972d0b1cf4831ffda90&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/361333424.hd.mp4?s=95995f98c6c1da90716fc972d0b1cf4831ffda90&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:456,&quot;series_id&quot;:102,&quot;position&quot;:13,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-20 11:46:28&quot;,&quot;updated_at&quot;:&quot;2019-09-20 11:46:28&quot;,&quot;published_at&quot;:&quot;2019-09-22 09:00:00&quot;,&quot;chapter&quot;:3,&quot;indices&quot;:[{&quot;id&quot;:3074,&quot;name&quot;:&quot;model eloquent&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1582}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1583,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Layout Pages&quot;,&quot;body&quot;:&quot;&lt;p&gt;If you review the &lt;code&gt;welcome&lt;\/code&gt; view that ships with Laravel, it contains the full HTML structure all the way up to the doctype. This is fine for a demo page, but in real life, you&#039;ll instead reach for layout files.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;layout-pages&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3394,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/361893007.hd.mp4?s=d5c821609411eb1c69c20042735912806fca4912&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/361893007.hd.mp4?s=d5c821609411eb1c69c20042735912806fca4912&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:251,&quot;series_id&quot;:102,&quot;position&quot;:14,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-23 16:01:32&quot;,&quot;updated_at&quot;:&quot;2019-09-23 16:01:32&quot;,&quot;published_at&quot;:&quot;2019-09-23 15:59:36&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1584,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Integrate a Site Template&quot;,&quot;body&quot;:&quot;&lt;p&gt;Using the techniques you&#039;ve learned in the last several episodes, let&#039;s integrate a free site template into our Laravel project, called &lt;a href=\&quot;https:\/\/templated.co\/simplework\&quot; target=\&quot;_blank\&quot;&gt;SimpleWork&lt;\/a&gt;.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;integrate-a-site-template&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3502,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/362050705.hd.mp4?s=8dfed2919e7a354a26f34e132ad40e97d4cff946&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/362050705.hd.mp4?s=8dfed2919e7a354a26f34e132ad40e97d4cff946&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:267,&quot;series_id&quot;:102,&quot;position&quot;:15,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-24 10:00:17&quot;,&quot;updated_at&quot;:&quot;2019-09-24 10:00:17&quot;,&quot;published_at&quot;:&quot;2019-09-24 09:59:27&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1585,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Set an Active Menu Link&quot;,&quot;body&quot;:&quot;&lt;p&gt;In this episode, you&#039;ll learn how to detect and highlight the current page in your navigation bar. We can use the &lt;code&gt;Request&lt;\/code&gt; facade for this.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;set-an-active-menu-link&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3555,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/362058122.hd.mp4?s=ac1721f0f501223bcf6a7e45182576bbc5a9aaff&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/362058122.hd.mp4?s=ac1721f0f501223bcf6a7e45182576bbc5a9aaff&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:135,&quot;series_id&quot;:102,&quot;position&quot;:16,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-24 10:26:00&quot;,&quot;updated_at&quot;:&quot;2019-09-24 10:26:00&quot;,&quot;published_at&quot;:&quot;2019-09-25 09:00:00&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[{&quot;id&quot;:3075,&quot;name&quot;:&quot;view&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1585},{&quot;id&quot;:3076,&quot;name&quot;:&quot;request&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1585}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1586,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Asset Compilation with Laravel Mix and webpack&quot;,&quot;body&quot;:&quot;&lt;p&gt;Laravel provides a useful tool called Mix - a wrapper around webpack - to assist with asset bundling and compilation. In this episode, I&#039;ll show you the basic workflow you&#039;ll follow when working on your frontend.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;asset-compilation-with-laravel-mix-and-webpack&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:3085,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/362389143.hd.mp4?s=9eb6b34d707fa17ead194b8e67cf682a66d31b38&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/362389143.hd.mp4?s=9eb6b34d707fa17ead194b8e67cf682a66d31b38&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:459,&quot;series_id&quot;:102,&quot;position&quot;:17,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-25 15:30:22&quot;,&quot;updated_at&quot;:&quot;2019-09-25 15:30:22&quot;,&quot;published_at&quot;:&quot;2019-09-26 09:00:00&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[{&quot;id&quot;:3077,&quot;name&quot;:&quot;webpack&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1586},{&quot;id&quot;:3078,&quot;name&quot;:&quot;mix&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1586}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1588,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Render Dynamic Data&quot;,&quot;body&quot;:&quot;&lt;p&gt;Let&#039;s next learn how to render dynamic data. The \&quot;about\&quot; page of the site template we&#039;re using contains a list of articles. Let&#039;s create a model for these, store some records in the database, and then render them dynamically on the page.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;render-dynamic-data&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:2616,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/362815730.hd.mp4?s=aa2d90f28be0d76f7c95e41b7f3cefd7a6d8b858&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/362815730.hd.mp4?s=aa2d90f28be0d76f7c95e41b7f3cefd7a6d8b858&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:379,&quot;series_id&quot;:102,&quot;position&quot;:18,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-27 11:09:43&quot;,&quot;updated_at&quot;:&quot;2019-09-27 11:09:43&quot;,&quot;published_at&quot;:&quot;2019-09-27 10:55:50&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1589,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Render Dynamic Data: Part 2&quot;,&quot;body&quot;:&quot;&lt;p&gt;Let&#039;s finish up this exercise by creating a dedicated page for viewing a full article.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;render-dynamic-data-part-2&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:2378,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/362819475.hd.mp4?s=fb7eacf0bf0ef924548cfa5c392e7d9b3382f03d&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/362819475.hd.mp4?s=fb7eacf0bf0ef924548cfa5c392e7d9b3382f03d&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:305,&quot;series_id&quot;:102,&quot;position&quot;:19,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-27 11:20:08&quot;,&quot;updated_at&quot;:&quot;2019-09-27 11:20:08&quot;,&quot;published_at&quot;:&quot;2019-09-28 09:00:00&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[{&quot;id&quot;:3081,&quot;name&quot;:&quot;view&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1589}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1590,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Homework Solutions&quot;,&quot;body&quot;:&quot;&lt;p&gt;Let&#039;s review the solution to the homework from the end of the previous episode. To display a list of articles, you&#039;ll need to create a matching route, a corresponding controller action, and the view to iterate over the articles and render them on the page.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;homework-solutions&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1915,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/363319824.hd.mp4?s=37e4ba7f4d1734c9afe29b195fbe682c5d1f26dc&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/363319824.hd.mp4?s=37e4ba7f4d1734c9afe29b195fbe682c5d1f26dc&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:165,&quot;series_id&quot;:102,&quot;position&quot;:20,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-30 10:58:20&quot;,&quot;updated_at&quot;:&quot;2019-09-30 10:58:20&quot;,&quot;published_at&quot;:&quot;2019-09-30 10:45:50&quot;,&quot;chapter&quot;:4,&quot;indices&quot;:[],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1592,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;The Seven Restful Controller Actions&quot;,&quot;body&quot;:&quot;&lt;p&gt;There are seven restful controller actions that you should become familiar with. In this episode, we&#039;ll review their names and when you would reach for them.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;the-seven-restful-controller-actions&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:2762,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/363331159.hd.mp4?s=05b83d66f542e864d5a6a91d26e78df9b0289cda&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/363331159.hd.mp4?s=05b83d66f542e864d5a6a91d26e78df9b0289cda&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:303,&quot;series_id&quot;:102,&quot;position&quot;:21,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-09-30 11:36:19&quot;,&quot;updated_at&quot;:&quot;2019-09-30 11:36:19&quot;,&quot;published_at&quot;:&quot;2019-10-01 09:00:00&quot;,&quot;chapter&quot;:5,&quot;indices&quot;:[{&quot;id&quot;:3084,&quot;name&quot;:&quot;controller&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1592},{&quot;id&quot;:3085,&quot;name&quot;:&quot;rest&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1592}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1593,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Restful Routing&quot;,&quot;body&quot;:&quot;&lt;p&gt;Now that you&#039;re familiar with resourceful controllers, let&#039;s switch back to the routing layer and review a RESTful approach for constructing URIs and communicating intent.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;restful-routing&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:2203,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/363884036.hd.mp4?s=ff976e7b2411c6f4bb557331575d923cec9b3031&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/363884036.hd.mp4?s=ff976e7b2411c6f4bb557331575d923cec9b3031&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:457,&quot;series_id&quot;:102,&quot;position&quot;:22,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-02 14:43:08&quot;,&quot;updated_at&quot;:&quot;2019-10-02 14:43:08&quot;,&quot;published_at&quot;:&quot;2019-10-02 14:34:32&quot;,&quot;chapter&quot;:5,&quot;indices&quot;:[{&quot;id&quot;:3086,&quot;name&quot;:&quot;route&quot;,&quotquot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1593},{&quot;id&quot;:3087,&quot;name&quot;:&quot;rest&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1593}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1594,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Form Handling&quot;,&quot;body&quot;:&quot;&lt;p&gt;Now that you understand resourceful controllers and HTTP verbs, let&#039;s build a form to persist a new article.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;form-handling&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1828,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/364127698.hd.mp4?s=0b539069bca284276f464aef34fd7bde74744ef5&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/364127698.hd.mp4?s=0b539069bca284276f464aef34fd7bde74744ef5&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:475,&quot;series_id&quot;:102,&quot;position&quot;:23,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-03 14:37:04&quot;,&quot;updated_at&quot;:&quot;2019-10-03 14:37:04&quot;,&quot;published_at&quot;:&quot;2019-10-03 14:35:32&quot;,&quot;chapter&quot;:5,&quot;indices&quot;:[],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1596,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Forms That Submit PUT Requests&quot;,&quot;body&quot;:&quot;&lt;p&gt;Browsers, at the time of this writing, only recognize &lt;code&gt;GET&lt;\/code&gt; and &lt;code&gt;POST&lt;\/code&gt; request types. No problem, though; we can get around this limitation by passing a hidden input along with our request that signals to Laravel which HTTP verb we actually want. Let&#039;s review the basic workflow in this episode.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;forms-that-submit-put-requests&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1778,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/364356199.hd.mp4?s=f3cdf659b2a2639c187223ff563c487f1f61bc6e&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/364356199.hd.mp4?s=f3cdf659b2a2639c187223ff563c487f1f61bc6e&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:395,&quot;series_id&quot;:102,&quot;position&quot;:24,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-04 14:06:25&quot;,&quot;updated_at&quot;:&quot;2019-10-04 14:06:25&quot;,&quot;published_at&quot;:&quot;2019-10-04 14:05:15&quot;,&quot;chapter&quot;:5,&quot;indices&quot;:[{&quot;id&quot;:3088,&quot;name&quot;:&quot;form&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1596},{&quot;id&quot;:3089,&quot;name&quot;:&quot;rest&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1596}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1598,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Form Validation Essentials&quot;,&quot;body&quot;:&quot;&lt;p&gt;Before we move on to cleaning up the controller, let&#039;s first take a moment to review form validation. At the moment, our controller doesn&#039;t care what the user types into each input. We assign each provided value to a property and attempt to throw it in the database. You should never do this. Remember: when dealing with user-provided data, assume that they&#039;re being malicious.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;form-validation-essentials&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1196,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/364932351.hd.mp4?s=04c53ffa668bc6fac2a540c1d591fbf0a4f911fd&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/364932351.hd.mp4?s=04c53ffa668bc6fac2a540c1d591fbf0a4f911fd&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:533,&quot;series_id&quot;:102,&quot;position&quot;:25,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-07 21:22:07&quot;,&quot;updated_at&quot;:&quot;2019-10-07 21:22:07&quot;,&quot;published_at&quot;:&quot;2019-10-07 21:13:51&quot;,&quot;chapter&quot;:5,&quot;indices&quot;:[{&quot;id&quot;:3092,&quot;name&quot;:&quot;validation&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1598},{&quot;id&quot;:3093,&quot;name&quot;:&quot;form&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1598}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1599,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Leverage Route Model Binding&quot;,&quot;body&quot;:&quot;&lt;p&gt;So far. we&#039;ve been manually fetching a record from the database using a wildcard from the URI. However, Laravel can perform this query for us automatically, thanks to route model binding.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;leverage-route-model-binding&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1167,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/365169461.hd.mp4?s=b2b151d89737eb88400048524c4fbba9cb51bc97&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/365169461.hd.mp4?s=b2b151d89737eb88400048524c4fbba9cb51bc97&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:296,&quot;series_id&quot;:102,&quot;position&quot;:26,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-08 20:52:29&quot;,&quot;updated_at&quot;:&quot;2019-10-08 20:52:29&quot;,&quot;published_at&quot;:&quot;2019-10-08 20:49:56&quot;,&quot;chapter&quot;:6,&quot;indices&quot;:[{&quot;id&quot;:3094,&quot;name&quot;:&quot;controller&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1599},{&quot;id&quot;:3095,&quot;name&quot;:&quot;refactor&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1599}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1600,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Reduce Duplication&quot;,&quot;body&quot;:&quot;&lt;p&gt;Your next technique is to reduce duplication. If you review our current&lt;code&gt;ArticlesController&lt;\/code&gt;, we reference request keys in multiple places. Now as it turns out, there&#039;s a useful way to reduce this repetition considerably.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;reduce-duplication&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1065,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/365174681.hd.mp4?s=c644f0a400a78858df66d9c60ee6f7291d77bcf7&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/365174681.hd.mp4?s=c644f0a400a78858df66d9c60ee6f7291d77bcf7&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:340,&quot;series_id&quot;:102,&quot;position&quot;:27,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-09 19:29:15&quot;,&quot;updated_at&quot;:&quot;2019-10-09 19:29:15&quot;,&quot;published_at&quot;:&quot;2019-10-09 19:26:37&quot;,&quot;chapter&quot;:6,&quot;indices&quot;:[{&quot;id&quot;:3096,&quot;name&quot;:&quot;controller&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1600},{&quot;id&quot;:3097,&quot;name&quot;:&quot;refactor&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1600}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}},{&quot;id&quot;:1601,&quot;type&quot;:&quot;episode&quot;,&quot;title&quot;:&quot;Consider Named Routes&quot;,&quot;body&quot;:&quot;&lt;p&gt;Named routes allow you to translate a URI into a variable. This way, if a route changes at some point down the road, all of your links will automatically update, due to the fact that they&#039;re referencing the named version of the route rather than the hardcoded path.&lt;\/p&gt;&quot;,&quot;free&quot;:1,&quot;slug&quot;:&quot;consider-named-routes&quot;,&quot;video_poster&quot;:&quot;&quot;,&quot;count&quot;:1097,&quot;download_sd&quot;:&quot;\/\/player.vimeo.com\/external\/365408730.hd.mp4?s=dbfe6922cb1aa00dc5fb78da91044004bab0bc6a&amp;profile_id=174&quot;,&quot;download&quot;:&quot;\/\/player.vimeo.com\/external\/365408730.hd.mp4?s=dbfe6922cb1aa00dc5fb78da91044004bab0bc6a&amp;profile_id=175&quot;,&quot;difficulty&quot;:0,&quot;experience&quot;:100,&quot;length&quot;:238,&quot;series_id&quot;:102,&quot;position&quot;:28,&quot;skill_id&quot;:1,&quot;created_at&quot;:&quot;2019-10-09 19:38:37&quot;,&quot;updated_at&quot;:&quot;2019-10-09 19:38:37&quot;,&quot;published_at&quot;:&quot;2019-10-10 09:00:00&quot;,&quot;chapter&quot;:6,&quot;indices&quot;:[{&quot;id&quot;:3098,&quot;name&quot;:&quot;controller&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1601},{&quot;id&quot;:3099,&quot;name&quot;:&quot;route&quot;,&quot;indexable_type&quot;:&quot;Laracasts\\Video&quot;,&quot;indexable_id&quot;:1601}],&quot;skill&quot;:{&quot;id&quot;:1,&quot;name&quot;:&quot;Laravel&quot;,&quot;abbreviation&quot;:&quot;L&quot;,&quot;theme&quot;:&quot;#ed6c63&quot;,&quot;description&quot;:&quot;Laravel is a PHP framework for constructing everything from small to enterprise-level applications. As you&#039;ll find, it&#039;s a joy to use, and just might make you enjoy writing PHP again. It&#039;s that good.&quot;,&quot;thumbnail&quot;:&quot;\/images\/series\/2018\/laravel-default.svg&quot;,&quot;created_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;updated_at&quot;:&quot;2015-10-29 04:39:57&quot;,&quot;path&quot;:&quot;\/skills\/laravel&quot;}}]}}" video-length=

<a href="/series/laravel-6-from-scratch/episodes/2?autoplay=true" class="video-card-next-button tw-flex tw-bg-grey-panel hover:tw-bg-grey-light tw-rounded-lg lg ▶
<svg xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38">

<g fill="none" fill-rule="evenodd">

<path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M1 37V1h36v36z"></path>

<path fill="#222" d="M13.76 25.885L20.63 19l-6.87-6.885L15.875 10l9 9-9 9z" opacity=".495"></path>

</g>

</svg>

</a>

</div>

<div class="tw-flex tw-mx-auto container" :style="expanded || ! isViewable ? {} : { maxWidth: '886px' }">

<div :class="expanded || ! isViewable ? 'lg:tw-mr-8' : ''">

<div class="tw-mb-8">

<h3 class="tw-bg-grey-panel tw-font-bold tw-px-4 tw-py-5 tw-rounded-lg tw-text-black tw-uppercase tw-mb-6 mobile:tw-text-lg">About This Episode</h3>

<div class="generic-content tw-text-black lg:tw-mx-8 mobile:tw-text-lg">

<p>Before we dig into the nuts and bolts of Laravel, let's first zoom out and discuss what exactly happens when a request comes in.</p>

<p class="tw-italic tw-text-sm tw-text-grey-dark">Published on Sep 9th, 2019.</p>

</div>

</div>

<div class="tw-my-5 lg:tw-hidden">

<a href="https://www.understand.io/?utm_source=laracasts&utm_medium=banner&utm_campaign=laracasts19" target="_blank" class="tw-mt-1 tw-mx-auto tw-block tw-curso ▶
<div class="tw-flex tw-items-center tw-bg-white tw-py-4 tw-px-5 tw-border tw-rounded" :class="expanded ? 'tw-border-dotted tw-border-grey-light' : 'tw-border-da ▶
<img src="/images/call-to-action/understand-io.png?v=2" alt="Understand.io logo" class="tw-mr-4" width="48" height="48">

<div class="tw-flex-1 tw-text-black">

<h5 class="lg:tw-text-sm tw-font-semibold tw-mb-2 hover:tw-text-blue">Laravel Error Tracking <span class="tw-text-3xs tw-text-grey-dark tw-relative tw-pl-1" sty ▶
<p class="tw-text-xs lg:tw-text-2xs tw-mb-2">

<strong class="hover:tw-text-blue">Understand.io</strong> is used by hundreds of Laravel developers to find and fix errors.

</p>

<p class="tw-text-xs lg:tw-text-2xs">

<strong class="hover:tw-text-blue">Sign up for a free 14-day trial now.</strong>

</p>

</div>

</div>

</a>

</div>

<div class="tw-mb-10">

<h3 class="tw-bg-grey-panel tw-font-bold tw-px-4 tw-py-5 tw-rounded-lg tw-text-black tw-uppercase tw-mb-6">Discuss It</h3>

<lesson-comments :video-id="1567" :mentionable-usernames="[&quot;jeffreyway&quot;,&quot;webrevllc&quot;,&quot;altrano&quot;,&quot;reymarktorres&quot;,&quot;nida ▶
<template v-slot:avatar>

<img src="https://s3.amazonaws.com/laracasts/images/forum/avatars/avatar-15.png" alt="Your avatar" class="tw-w-10 tw-h-10 tw-rounded-full tw-mr-4">

</template>

</lesson-comments>

</div>

<div class="lg:tw-hidden">

<h3 class="tw-bg-grey-panel tw-font-bold tw-px-4 tw-py-5 tw-rounded-lg tw-text-black tw-uppercase tw-mb-6">Episodes</h3>

<ol class="episode-list is-condensed" v-cloak>

<episode-list-chapter :chapter="1" :episode-count="4" inline-template>

<li id="chapter-1">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 1</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Prerequisites

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1567" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="01" :is-scheduled="false" skill="laravel" :id="1567" :is-current="true" :completed="false" progress- ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/1" class="tw-text-blue hover:tw-text-blue tw-font-bold" style="overflow:hidden; width: 225px; text-overflow: el ▶
At a Glance

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 1</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1569" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="02" :is-scheduled="false" skill="laravel" :id="1569" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/2" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Install PHP, MySQL and Composer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 2</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:33

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1570" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="03" :is-scheduled="false" skill="laravel" :id="1570" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/3" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
The Laravel Installer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 3</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:02

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1571" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="04" :is-scheduled="false" skill="laravel" :id="1571" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/4" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Laravel Valet Setup

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 4</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:18

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="2" :episode-count="4" inline-template>

<li id="chapter-2">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 2</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Routing

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1573" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="05" :is-scheduled="false" skill="laravel" :id="1573" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/5" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Basic Routing and Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 5</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:41

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1574" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="06" :is-scheduled="false" skill="laravel" :id="1574" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/6" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Pass Request Data to Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 6</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1575" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="07" :is-scheduled="false" skill="laravel" :id="1575" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/7" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Route Wildcards

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 7</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:42

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1576" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="08" :is-scheduled="false" skill="laravel" :id="1576" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/8" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Routing to Controllers

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 8</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:01

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="3" :episode-count="5" inline-template>

<li id="chapter-3">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 3</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Database Access

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1578" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="09" :is-scheduled="false" skill="laravel" :id="1578" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/9" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Setup a Database Connection

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 9</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:13

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1579" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="10" :is-scheduled="false" skill="laravel" :id="1579" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/10" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Hello Eloquent

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 10</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1580" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="11" :is-scheduled="false" skill="laravel" :id="1580" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/11" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Migrations 101

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 11</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:23

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1581" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="12" :is-scheduled="false" skill="laravel" :id="1581" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/12" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Generate Multiple Files in a Single Command

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 12</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

1:26

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1582" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="13" :is-scheduled="false" skill="laravel" :id="1582" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/13" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Business Logic

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 13</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:36

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="4" :episode-count="7" inline-template>

<li id="chapter-4">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 4</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Views

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

 <g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1583" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="14" :is-scheduled="false" skill="laravel" :id="1583" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/14" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Layout Pages

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 14</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1584" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="15" :is-scheduled="false" skill="laravel" :id="1584" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/15" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Integrate a Site Template

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 15</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:27

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1585" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="16" :is-scheduled="false" skill="laravel" :id="1585" :is-current="false" :completed="false" progress-key="/series/laravel-6-from-scratch/episodes/16-progress" @episode-completion-toggled="onCompletionUpdate"></series-complete-button>

</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/16" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Set an Active Menu Link

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 16</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:15

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1586" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="17" :is-scheduled="false" skill="laravel" :id="1586" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/17" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Asset Compilation with Laravel Mix and webpack

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 17</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:39

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1588" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="18" :is-scheduled="false" skill="laravel" :id="1588" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

 <a href="/series/laravel-6-from-scratch/episodes/18" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; w ▶
Render Dynamic Data

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 18</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:19

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1589" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="19" :is-scheduled="false" skill="laravel" :id="1589" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/19" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Render Dynamic Data: Part 2

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 19</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:05

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1590" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="20" :is-scheduled="false" skill="laravel" :id="1590" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/20" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Homework Solutions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 20</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="5" :episode-count="5" inline-template>

<li id="chapter-5">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 5</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Forms

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1592" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="21" :is-scheduled="false" skill="laravel" :id="1592" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/21" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
The Seven Restful Controller Actions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 21</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:03

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1593" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="22" :is-scheduled="false" skill="laravel" :id="1593" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/22" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Restful Routing

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 22</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:37

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1594" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="23" :is-scheduled="false" skill="laravel" :id="1594" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/23" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Handling

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 23</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:55

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1596" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="24" :is-scheduled="false" skill="laravel" :id="1596" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/24" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Forms That Submit PUT Requests

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 24</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:35

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1598" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="25" :is-scheduled="false" skill="laravel" :id="1598" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/25" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Validation Essentials

 </a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 25</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

8:53

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="6" :episode-count="3" inline-template>

<li id="chapter-6">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 6</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Controller Techniques

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

 </div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1599" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="26" :is-scheduled="false" skill="laravel" :id="1599" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/26" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Leverage Route Model Binding

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 26</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:56

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1600" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="27" :is-scheduled="false" skill="laravel" :id="1600" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/27" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Reduce Duplication

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 27</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1601" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="28" :is-scheduled="false" skill="laravel" :id="1601" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/28" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Consider Named Routes

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 28</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:58

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

</ol> </div>

</div>

<div v-show="expanded || ! isViewable" class="js-video-episode-list tw-hidden lg:tw-block tw-overflow-auto tw-flex-shrink-0 tw--mt-2 tw-z-10" style="width: 331p ▶
<div class="tw-mr-4" style="width: 331px;" :style="expanded || ! isViewable ? 'max-height: 1000px' : 'height: 0'" v-cloak>

<a href="https://www.understand.io/?utm_source=laracasts&utm_medium=banner&utm_campaign=laracasts19" target="_blank" class="tw-mt-1 tw-mx-auto tw-block tw-curso ▶
<div class="tw-flex tw-items-center tw-bg-white tw-py-4 tw-px-5 tw-border tw-rounded" :class="expanded ? 'tw-border-dotted tw-border-grey-light' : 'tw-border-da ▶
<img src="/images/call-to-action/understand-io.png?v=2" alt="Understand.io logo" class="tw-mr-4" width="48" height="48">

<div class="tw-flex-1 tw-text-black">

<h5 class="lg:tw-text-sm tw-font-semibold tw-mb-2 hover:tw-text-blue">Laravel Error Tracking <span class="tw-text-3xs tw-text-grey-dark tw-relative tw-pl-1" sty ▶
<p class="tw-text-xs lg:tw-text-2xs tw-mb-2">

<strong class="hover:tw-text-blue">Understand.io</strong> is used by hundreds of Laravel developers to find and fix errors.

</p>

<p class="tw-text-xs lg:tw-text-2xs">

<strong class="hover:tw-text-blue">Sign up for a free 14-day trial now.</strong>

</p>

</div>

</div>

</a>

<aside class="tw-hidden lg:tw-block tw-bg-white tw-p-2 tw-rounded-lg tw-overflow-y-auto" :class="expanded ? 'tw-border tw-border-dotted tw-border-grey-light tw- ▶
<header class="sidebar-series-card tw-transition-all hover:tw-bg-laravel tw-rounded-lg tw-cursor-pointer">

<a href="/series/laravel-6-from-scratch" class="tw-block tw-flex tw-items-center tw-py-3 tw-px-4">

<img src="/images/series/2018/laravel-6-from-scratch.svg" alt="Laravel 6 From Scratch" width="48" height="48" class="tw-mr-3 tw-flex-shrink-0">

<div>

<h2 class="sidebar-series-card-title tw-font-bold tw-mb-2 tw-leading-none tw-text-black">Laravel 6 From Scratch</h2>

<div class="tw-flex tw-items-center tw-text-3xs">

<div class="tw-flex tw-items-center tw-leading-none">

<strong class="sidebar-series-card-series-difficulty tw-mr-2 group-hover:tw-text-white">SERIES</strong>

<div class="tw-mr-4 difficulty-meter tw-flex tw-flex-row-reverse is-intermediate">

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px; margin-left: 3px;"></span>

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px; margin-left: 3px;"></span>

<span class="tw-rounded-lg tw-block" style="width: 6px; height: 9px;"></span>

</div>

</div>

<div class="tw-inline-flex tw-items-center tw-mr-2">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="11" viewBox="0 0 9 11" class="sidebar-series-card-icon tw-text-grey-dark tw-mr-1">

<path class="tw-fill-current" fill-rule="nonzero" d="M2.642 4.394v3.768a.393.393 0 0 1-.252.372c-.214.079-.472.12-.747.12-.79 0-1.641-.338-1.641-1.081V2.751c-.0 ▶
</svg>

<strong class="sidebar-series-card-stat tw-text-grey-dark">29 Lessons</strong>

</div>

<div class="tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8" class="sidebar-series-card-icon tw-text-grey-dark tw-relative" style="margin-righ ▶
<path class="tw-fill-current" fill-rule="evenodd" d="M4 0C1.8 0 0 1.8 0 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm1.826 5.538L3.692 4.254V1.846h.616v2.087l1.846 1.084-.3 ▶
</svg>

<strong class="sidebar-series-card-stat tw-text-grey-dark">2:21:24 hrs</strong>

</div>

</div>

</div>

</a>

</header>

<hr style="margin-top: 0; margin-bottom: 8px;" class="tw-border-grey-lighter">

<ol class="episode-list is-condensed" v-cloak>

<episode-list-chapter :chapter="1" :episode-count="4" inline-template>

<li id="chapter-1">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 1</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Prerequisites

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1567" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="01" :is-scheduled="false" skill="laravel" :id="1567" :is-current="true" :completed="false" progress- ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/1" class="tw-text-blue hover:tw-text-blue tw-font-bold" style="overflow:hidden; width: 225px; text-overflow: el ▶
At a Glance

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 1</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1569" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="02" :is-scheduled="false" skill="laravel" :id="1569" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/2" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Install PHP, MySQL and Composer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 2</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:33

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1570" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="03" :is-scheduled="false" skill="laravel" :id="1570" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/3" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
The Laravel Installer

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 3</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:02

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1571" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="04" :is-scheduled="false" skill="laravel" :id="1571" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/4" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Laravel Valet Setup

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 4</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:18

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="2" :episode-count="4" inline-template>

<li id="chapter-2">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 2</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Routing

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1573" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="05" :is-scheduled="false" skill="laravel" :id="1573" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/5" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Basic Routing and Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 5</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:41

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1574" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="06" :is-scheduled="false" skill="laravel" :id="1574" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/6" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Pass Request Data to Views

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 6</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1575" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="07" :is-scheduled="false" skill="laravel" :id="1575" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/7" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Route Wildcards

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 7</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:42

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1576" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="08" :is-scheduled="false" skill="laravel" :id="1576" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/8" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Routing to Controllers

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 8</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:01

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="3" :episode-count="5" inline-template>

<li id="chapter-3">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 3</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Database Access

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1578" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="09" :is-scheduled="false" skill="laravel" :id="1578" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/9" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; whi ▶
Setup a Database Connection

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 9</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:13

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1579" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="10" :is-scheduled="false" skill="laravel" :id="1579" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/10" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Hello Eloquent

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 10</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1580" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="11" :is-scheduled="false" skill="laravel" :id="1580" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/11" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Migrations 101

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 11</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:23

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1581" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="12" :is-scheduled="false" skill="laravel" :id="1581" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/12" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Generate Multiple Files in a Single Command

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 12</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

1:26

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1582" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="13" :is-scheduled="false" skill="laravel" :id="1582" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/13" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Business Logic

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 13</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:36

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="4" :episode-count="7" inline-template>

<li id="chapter-4">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 4</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Views

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1583" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="14" :is-scheduled="false" skill="laravel" :id="1583" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/14" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Layout Pages

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 14</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:11

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1584" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="15" :is-scheduled="false" skill="laravel" :id="1584" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/15" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Integrate a Site Template

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 15</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:27

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1585" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="16" :is-scheduled="false" skill="laravel" :id="1585" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/16" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Set an Active Menu Link

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 16</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:15

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1586" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="17" :is-scheduled="false" skill="laravel" :id="1586" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/17" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Asset Compilation with Laravel Mix and webpack

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 17</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:39

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1588" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="18" :is-scheduled="false" skill="laravel" :id="1588" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/18" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Render Dynamic Data

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 18</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:19

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1589" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="19" :is-scheduled="false" skill="laravel" :id="1589" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/19" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Render Dynamic Data: Part 2

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 19</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:05

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1590" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="20" :is-scheduled="false" skill="laravel" :id="1590" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/20" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Homework Solutions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 20</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

2:45

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="5" :episode-count="5" inline-template>

<li id="chapter-5">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 5</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Forms

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1592" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="21" :is-scheduled="false" skill="laravel" :id="1592" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/21" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
The Seven Restful Controller Actions

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 21</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

 </svg>

5:03

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1593" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="22" :is-scheduled="false" skill="laravel" :id="1593" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/22" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Restful Routing

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 22</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:37

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1594" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="23" :is-scheduled="false" skill="laravel" :id="1594" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/23" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Handling

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 23</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

7:55

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1596" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="24" :is-scheduled="false" skill="laravel" :id="1596" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/24" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Forms That Submit PUT Requests

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 24</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

6:35

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
 </div>

</div>

</div>

</li>

<li id="episode-1598" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="25" :is-scheduled="false" skill="laravel" :id="1598" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/25" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Form Validation Essentials

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 25</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

8:53

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

<episode-list-chapter :chapter="6" :episode-count="3" inline-template>

<li id="chapter-6">

<div class="tw-relative tw-flex tw-items-center tw-w-full tw-bg-grey-panel tw-rounded-lg tw-px-4 tw-py-2 tw-cursor-pointer" :class="isOpen ? '' : 'tw-mb-3'" @mo ▶
<h2 class="tw-font-bold tw-text-sm tw-text-black">

<span class="tw-text-sm">Section 6</span>

<span class="tw-w-px tw-inline-block tw-mx-2 lg:tw-mx-4 tw-bg-grey-light tw-align-middle" style="height: 30px"></span>

Controller Techniques

</h2>

<div v-show="allEpisodesComplete" class="circle tw-w-5 tw-h-5 tw-ml-3 tw-flex tw-bg-grey-light tw-items-center tw-bg-laravel tw-text-laravel">

<a href="#" class="position tw-text-white hover:tw-text-black md:tw-text-lg tw-w-full tw-h-full tw-flex tw-items-center tw-justify-center">

<svg width="40%" height="100%" viewBox="0 0 21 16" class="tw-inline-block tw-relative" style="top: .7px;">

<g fill="#FFF" fill-rule="evenodd">

<path fill="none" d="M-3-5h27v27H-3z"></path>

<path d="M7.439 12.152l-5.037-5.36c-.447-.477-1.119-.477-1.566 0a1.204 1.204 0 0 0 0 1.667l6.603 7.03L20.086 2.025a1.204 1.204 0 0 0 0-1.668c-.447-.476-1.12-.47 ▶
</g>

</svg>

</a>

</div>

<button class="tw-p-3 tw-absolute tw-right-0 tw-mr-4" :class="{

                                            'tw-bg-black-transparent-3 tw-rounded-lg' : isHovered,

                                            'tw-opacity-100': (! isOpen || isHovered),

                                            'tw-opacity-0': (isOpen || ! isHovered)

                                        }" style="transition: opacity .3s">

<template v-if="isOpen">

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".497">

<path stroke="#000" stroke-opacity=".012" stroke-width="0" d="M-6-7h24v24H-6z"></path>

<path fill="#000" d="M1.41.84L6 5.42 10.59.84 12 2.25l-6 6-6-6z"></path>

</g>

</svg>

</template>

<template v-else>

<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">

<g fill="none" fill-rule="evenodd" opacity=".401">

<path d="M18 16H-6V-8h24z"></path>

<path fill="#000" d="M10.59 8.16L6 3.58 1.41 8.16 0 6.75l6-6 6 6z"></path>

</g>

</svg>

</template>

</button>

</div>

<ol class="tw-py-2" v-show="isOpen">

<li id="episode-1599" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="26" :is-scheduled="false" skill="laravel" :id="1599" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/26" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Leverage Route Model Binding

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 26</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

4:56

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1600" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="27" :is-scheduled="false" skill="laravel" :id="1600" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1  mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/27" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Reduce Duplication

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 27</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

5:40

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

<li id="episode-1601" class="content-item episode-list-item tw-cursor-pointer tw-py-2 tw-px-3 tw-items-center   is-laravel" @click="goToEpisode">

<div class="episode-list-index tw-relative tw-items-center tw-mr-4">

<series-complete-button :condensed="true" episode-position="28" :is-scheduled="false" skill="laravel" :id="1601" :is-current="false" :completed="false" progress ▶
</div>

<div class="episode-list-details tw-flex-1 tw-border-0 mobile:tw-border-b-0">

<div class="">

<h4 class="episode-list-title tw-mb-1 tw-text-sm tw-font-semibold tw-flex tw-items-center tw-mr-3">

<a href="/series/laravel-6-from-scratch/episodes/28" class="tw-text-black hover:tw-text-black" style="overflow:hidden; width: 225px; text-overflow: ellipsis; wh ▶
Consider Named Routes

</a>

</h4>

<div class="tw-flex tw-leading-none tw-items-center">

<span class="tw-text-3xs tw-mr-2 tw-font-bold tw-text-grey-40">EPISODE 28</span>

<strong class="tw-text-grey-40 tw-text-3xs tw-font-semibold tw-inline-flex tw-items-center">

<svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 13 13" class="tw-mr-1 tw-fill-current tw-text-grey-40 tw-opacity-50">

<path fill-rule="evenodd" d="M6.5 0C2.925 0 0 2.925 0 6.5S2.925 13 6.5 13 13 10.075 13 6.5 10.075 0 6.5 0zm2.967 9L6 6.913V3h1v3.391l3 1.761L9.467 9z"></path>

</svg>

3:58

</strong>

<span class="tw-border tw-border-solid tw-border-blue tw-text-blue tw-text-3xs tw-px-1 tw-py-px tw-uppercase tw-rounded-full tw-font-bold tw-ml-2 tw-tracking-wi ▶
</div>

</div>

</div>

</li>

</ol>

</li>

</episode-list-chapter>

</ol> </aside>

</div>

</div>

</div>

</div>

</div>

</div>

<a href="https://laracasts.com/series/laravel-6-from-scratch" class="lg:tw-hidden tw-rounded-full tw-z-50 tw-w-16 tw-h-16 tw-text-center tw-flex tw-items-center ▶
<img src="/images/mobile-back-button.svg?v=2" alt="Back to Series Button" class="tw-rounded-full tw-bg-white">

</a>

</div>

</lesson-view>

<section class="tw-hidden md:tw-block signup-banner tw-text-white tw-py-0">

<div class="container tw-mx-auto">

<div class="tw-flex tw-justify-center tw-items-center tw-h-full">

<div class="tw-mr-9">

<img src="/images/call-to-action/robot-illustration.svg?v=2" style="mix-blend-mode: luminosity" alt="Laracasts Sign Up Mascot">

</div>

<div class="tw-w-1/2">

<h3 class="tw-text-3xl tw-mb-6">

The most concise screencasts for the working developer, updated daily.

</h3>

<p class="tw-mb-10">

There's no shortage of content at Laracasts. In fact, you could watch nonstop

for days upon days, and still not see everything!

</p>

<p>

<a href="/join" @click.prevent="modal.show('join-modal')" class="tw-btn tw-btn-outlined tw-text-white tw-inline-block hover:tw-bg-white hover:tw-text-blue hove ▶
Get Started

</a>

</p>

</div>

</div>

</div>

</section>

<div class="footer-wrap mobile:tw-text-lg">

<section class="footer-section top md:tw-pb-0 tw-pt-0">

<div class="container">

<div class="md:tw-w-4/5 lg:tw-w-1/2 tw-mx-auto tw-text-center">

<div>

<h2 class="tw-text-white tw-text-3xl pr-1-tablet tw-mb-8 tw-tracking-tight">

Want us to email you occasionally with Laracasts news?

</h2>

</div>

<div>

<newsletter-form></newsletter-form>

</div>

</div>

</div>

</section>

<section class="footer-section bottom tw-text-center md:tw-text-left tw-py-0">

<footer class="container">

<div class="md:tw-flex md:mb-3">

<div class="md:tw-w-2/5 md:tw-mr-auto">

<div class="tw-mb-3 tw-flex tw-justify-center md:tw-block">

<svg class="tw-fill-current tw-text-white" width="232" height="44" viewBox="0 0 323 36" xmlns="http://www.w3.org/2000/svg">

<g fill="none" fill-rule="evenodd">

<path d="M0 4.83h6.986v24.521h15.137v5.699H0V4.83zm53.374 23.744H39.36l-2.674 6.476h-7.159L42.981 4.83h6.9L63.38 35.05h-7.33l-2.675-6.476zm-2.199-5.31l-4.787-11 ▶
<g fill="#FFF">

<path d="M192.912 8.921l4.923 4.924a2.597 2.597 0 0 1-3.673 3.673l-4.923-4.924a2.597 2.597 0 1 1 3.673-3.673zM203.437 19.447l10.547 10.547a2.597 2.597 0 0 1-3.6 ▶
<path d="M190.416 8.255L218.753.893A2.672 2.672 0 0 1 222 2.767a2.592 2.592 0 0 1-1.846 3.199l-28.338 7.362a2.672 2.672 0 0 1-3.247-1.875 2.592 2.592 0 0 1 1.84 ▶
<path d="M209.592 31.104l7.363-28.337A2.592 2.592 0 0 1 220.153.92a2.672 2.672 0 0 1 1.875 3.247l-7.362 28.337a2.592 2.592 0 0 1-3.199 1.847 2.672 2.672 0 0 1-1 ▶
</g>

<path d="M208.62 18.067l-10.236 10.235a2.643 2.643 0 1 1-3.738-3.738l10.236-10.235a2.643 2.643 0 0 1 3.738 3.738zM193.38 33.306l-1.54 1.54a2.643 2.643 0 1 1-3.7 ▶
</g>

</svg>

</div>

<p class="tw-mb-8 tw-text-white tw-leading-loose">

Nine out of ten doctors recommend Laracasts over competing brands.

Come inside, see for yourself, and massively level up your development skills in the process.

</p>

<div class="tw-flex tw-w-42 md:tw-w-28 tw-justify-between tw-items-center phone:tw-m-auto mobile:tw-mb-8">

<a href="https://www.youtube.com/laracastsofficial" target="_blank" rel="noreferrer">

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 19" class="tw-w-8 md:tw-w-6 tw-text-white hover:tw-text-black tw-transition-all">

<g class="tw-fill-current" fill-rule="nonzero">

<path d="M6.4 0H5.371l-.685 2.629L4 0H2.857c.229.686.457 1.257.686 1.943.343.914.571 1.714.571 2.171v2.743h1.029V4.114L6.4 0zm2.743 5.143V3.429c0-.572-.114-.915 ▶
<path d="M15.314 9.486C15.086 8.686 14.4 8 13.714 8 11.886 7.771 9.943 7.771 8 7.771c-1.943 0-3.771 0-5.714.229-.686 0-1.372.686-1.6 1.486-.229 1.143-.229 2.4-. ▶
</g>

</svg>

</a>

<a href="https://twitter.com/laracasts" target="_blank" rel="noreferrer">

<svg xmlns="http://www.w3.org/2000/svg" class="tw-w-8 md:tw-w-6 tw-text-white hover:tw-text-black tw-transition-all" viewBox="0 0 18 18">

<path class="tw-fill-current" fill-rule="nonzero" d="M9 0C4.037 0 0 4.037 0 9c0 4.962 4.037 9 9 9 4.962 0 9-4.038 9-9 0-4.963-4.037-9-9-9zm4.015 6.94c.004.09.00 ▶
</svg>

</a>

<a href="https://github.com/laracasts" target="_blank" rel="noreferrer">

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 29" class="tw-w-8 md:tw-w-6 tw-text-white hover:tw-text-black tw-transition-all">

<path class="tw-fill-current" fill-rule="nonzero" d="M27.959 7.434a14.866 14.866 0 0 0-5.453-5.414C20.21.69 17.703.025 14.984.025c-2.718 0-5.226.665-7.521 1.995 ▶
</svg>

</a>

</div>

</div>

<div class="md:tw-w-1/6 mobile:tw-mb-6">

<h5 class="tw-text-white tw-font-bold md:tw-font-semibold tw-mb-2 md:tw-mb-6 tw-uppercase">Learn</h5>

<ul class="tw-leading-loose">

<li>

<a href="/join" class="tw-text-transparent-50 hover:tw-text-white" @click.prevent="modal.show('join-modal')">Sign Up</a>

</li>

<li>

<a class="tw-text-transparent-50 hover:tw-text-white" href="/login" @click.prevent="dispatch('Login')">

Sign In

</a>

</li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/search?refinement=type&name=series">Browse</a></li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/index">Lesson Index</a></li>

</ul>

</div>

<div class="md:tw-w-1/6 mobile:tw-mb-6">

<h5 class="tw-text-white tw-font-bold md:tw-font-semibold tw-mb-2 md:tw-mb-6 tw-uppercase">Discuss</h5>

<ul class="tw-leading-loose">

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/discuss">Forum</a></li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/podcast">Podcast</a></li>

<li><support-button></support-button></li>

</ul>

</div>

<div class="md:tw-w-1/6 mobile:tw-mb-8">

<h5 class="tw-text-white tw-font-bold md:tw-font-semibold tw-mb-2 md:tw-mb-6 tw-uppercase">Extras</h5>

<ul class="tw-leading-loose">

<li>

<a class="tw-text-transparent-50 hover:tw-text-white" @click.prevent="modal.show('testimonials-modal')">Testimonials</a>

</li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/faq">FAQ</a></li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="https://assets.laracasts.com">Assets</a></li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="https://larajobs.com/?partner=36#" target="_blank" rel="noreferrer">Get a Job</a></li>

</ul>

<ul class="zeroed tw-leading-loose">

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/privacy">Privacy</a></li>

<li><a class="tw-text-transparent-50 hover:tw-text-white" href="/terms">Terms</a></li>

</ul>

</div>

</div>

<div class="tw-border-t tw-border-solid tw-border-transparent-10 tw-py-4 md:tw-mt-8 mobile:tw-text-lg">

<div class="tw-text-transparent-50 tw-text-base md:tw-text-xs tw-text-center">

<p class="tw-mb-5 md:tw-mb-2 tw-tracking-normal">

&copy; Laracasts 2019. All rights reserved. <br class="md:tw-hidden" />Yes, all of them. That means you, Todd.

</p>

<p class="tw-tracking-normal">

<span class="mobile:tw-block">Designed with <img src="/images/icons/heart.svg" alt="heart" class="tw-px-1 tw-relative heart-pulse tw-inline" style="top: 2px" lo ▶
<br class="md:tw-hidden">Proudly hosted with <a href="https://forge.laravel.com" class="tw-font-bold inherits-color link hover:tw-text-white">Laravel Forge</a>\ ▶
and <a href="https://www.digitalocean.com/?refcode=d2070a2d5f35" class="tw-font-bold link inherits-color link hover:tw-text-white">DigitalOcean</a>.

</p>

</div>

</div>

</footer>

</section>

</div>

</div>

<login-modal token="2Z1AeqXy7gJOKxtHHVMc90FabPKTg7nLlO1pBAEj"></login-modal>

<join initial-category="personalPlans"></join>

<testimonials-modal></testimonials-modal>

<search-modal></search-modal>

</div> 

<script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script type="44f9887cfb01aead5e918449-text/javascript">

    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=

        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;

        e=o.createElement(i);r=o.getElementsByTagName(i)[0];

        e.src='//www.google-analytics.com/analytics.js';

        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));

    ga('create','UA-44120322-1');

</script>

<script src="https://player.vimeo.com/api/player.js" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="/highlight/highlight.pack.js" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="/js/manifest.js?id=227795a4cf81cedbd601" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="/js/vendor.js?id=65f9c83098145e784c22" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script src="/js/app.js?id=449acafd6ccce88a7b64" type="44f9887cfb01aead5e918449-text/javascript"></script>

<script type="44f9887cfb01aead5e918449-text/javascript">

    ga('send', 'pageview');

</script>

<script type="44f9887cfb01aead5e918449-text/javascript">

    var el = document.querySelectorAll('img');

    var observer = window.lozad(el);

    observer.observe();

</script>

<script type="44f9887cfb01aead5e918449-text/javascript">

    

    

    

    </script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="44f9887cfb01aead5e918449-|49" defer= ▶
</html>
HTML;
    }
}
