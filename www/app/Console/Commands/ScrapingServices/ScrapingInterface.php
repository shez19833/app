<?php

namespace App\Console\Commands\ScrapingServices;

use App\Models\Provider;

interface ScrapingInterface {
    public function handle(Provider $provider);
}

