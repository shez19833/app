<?php

namespace App\Console\Commands;

use App\Console\Commands\ScrapingServices\CodecourseScraper;
use App\Console\Commands\ScrapingServices\LaracastScraper;
use App\Models\Provider;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class ScrapingProviders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'z:scrape-providers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'scrape Providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Provider::all()->each(function ($provider){
            info('Scraping:', [$provider->name]);

            $service = $this->scrapingService($provider);
            $service->handle($provider);
        });
    }

    /**
     * @param $provider
     * @return CodecourseScraper|LaracastScraper
     * @throws Exception
     */
    public function scrapingService($provider)
    {
        switch ($provider->name) {
            case Provider::LARACASTS:
                return new LaracastScraper(new Client());
            case Provider::CODECOURSE:
                return new CodecourseScraper(new Client());
            default:
                throw new Exception("No Scraping Service for given provider");
        }
    }
}
