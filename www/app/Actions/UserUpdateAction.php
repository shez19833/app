<?php


namespace App\Actions;


use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserUpdateAction
{
    /**
     * Update a User
     *
     * @param User $user
     * @param array $data
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function handle(User $user, array $data)
    {
        $data = array_filter($data);

        $user->fill(Arr::only($data, ['name', 'email', 'wants_newsletter']));

        if ( !isset($data['wants_newsletter']) ) {
            $user->wants_newsletter = 0;
        }

        if ( isset($data['new_password']) ) {
            $user->password = Hash::make($data['new_password']);
        }

        if ( $user->isACustomer() && isset($data['payment_method']) ) {
            $paymentMethod = $user->defaultPaymentMethod();
            $paymentMethod->delete();

            $user->updateDefaultPaymentMethod($data['payment_method']);
        }

        if ( $user->isAProvider() ) {
            $user->loadMissing('provider');

            $user->provider->fill([
                'name' => $data['provider_name'],
                'description' => $data['provider_description'],
                'url' => $data['provider_url'],
            ]);
            $user->provider->save();
        }

        $user->save();
    }
}
