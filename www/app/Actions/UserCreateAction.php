<?php


namespace App\Actions;


use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;

class UserCreateAction
{
    /**
     * Create A User
     *
     * @param array $data
     * @return User
     */
    public function handle(array $data) : User
    {
        $user = User::create($data);

        if ( !isset($data['wants_newsletter']) ) {
            $user->wants_newsletter = 0;
        }

        $user->password = Hash::make($data['password']);

        if ($user->isACustomer()) {
            $user->createOrGetStripeCustomer();
            $user->addPaymentMethod($data['payment_method']);
            $user->newSubscription('main', config('cashier.subscripion_id'))
                ->create($data['payment_method']);
        }

        $user->save();

        event(new Registered($user));

        return $user;
    }
}
