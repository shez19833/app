<?php

namespace App\Listeners;

use App\Events\StripeIncomingPayment;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class createPaymentRecord
{
    /**
     * Handle the event.
     *
     * @param  StripeIncomingPayment  $event
     * @return void
     */
    public function handle(StripeIncomingPayment $event)
    {
        $payload = $event->payload;
        $object = $payload['data']['object'];

        if ( $payload['type'] !== 'charge.succeeded' ) {
            Log::error('Invalid Payload Type encountered during createPaymentRecord'
                , $payload);
            return;
        }

        $user = User::whereStripeId($object['customer'])
            ->first();

        if ( ! $user ) {
            Log::error('User not found during createPaymentRecord', $object);
            return;
        }

        $data = [
            'total' => $object['amount'],
            'user_id' => $user->id,
            'paid_at' => Carbon::createFromTimestamp($object['created']),
        ];

        if ( Payment::whereUserId($user->id)
                    ->wherePaidAt($data['paid_at'])
                    ->exists()
        ) {
            Log::error('Payment Already exists:', $object);
            return;
        }

        Payment::create($data);
    }
}
