<?php

/*
 * Tested
 */
function formatDuration($duration)
{
    $hours = floor($duration / 3600);
    $minutes = floor(($duration / 60) % 60);

    $return = [];

    switch($hours) {
        case 0: break;
        case 1: $return[] = '1 hour'; break;
        default: $return[] = "$hours hours"; break;
    }

    switch ($minutes) {
        case 0: break;
        case 1: $return[] = '1 minute'; break;
        default: $return[] = "$minutes minutes"; break;
    }

    return implode(" and ", $return);
}
