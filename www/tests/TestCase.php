<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        \VCR\VCR::configure()->setCassettePath(__DIR__ .'/fixtures');
        \VCR\VCR::configure()->enableLibraryHooks('curl');
        \VCR\VCR::turnOn();
        \VCR\VCR::insertCassette('example.yml');
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        \VCR\VCR::eject();
        \VCR\VCR::turnOff();
    }

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        $this->artisan('migrate');
        $this->artisan('db:seed', ['--class' => 'UserTypesTableSeeder']);
        $this->artisan('db:seed', ['--class' => 'SettingsTableSeeder']);

        $this->app[Kernel::class]->setArtisan(null);
    }
}
