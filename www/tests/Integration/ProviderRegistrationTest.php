<?php

namespace Tests\Unit;

use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProviderRegistrationTest extends TestCase
{
    use DatabaseTransactions;

    public function test_a_provider_can_register()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'type_id' => UserType::PROVIDER,
        ];

        // act
        $this->post('register', $data);

        // assert
        $this->assertDatabaseHas('users', [
            'name' => $data['name'],
            'email' => $data['email'],
            'card_brand' => null,
            'card_last_four' => null,
            'type_id' => UserType::PROVIDER,
        ]);
    }
}
