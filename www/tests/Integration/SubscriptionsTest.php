<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Cashier\Subscription;
use Tests\TestCase;

class SubscriptionsTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();


    }

    public function test_i_can_cancel_my_subscription()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'payment_method' => 'pm_card_visa',
            'type_id' => UserType::CUSTOMER,
        ];

        // act
        $this->post('register', $data);

        $user = User::whereEmail($data['email'])->first();
        $user->subscription('main')->cancel();

        // assert
        $subscription = Subscription::whereUserId($user->id)->first();
        $this->assertNotNull($subscription->ends_at);
    }

    public function test_i_can_renew_my_subscription()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'payment_method' => 'pm_card_visa',
            'type_id' => UserType::CUSTOMER,
        ];

        // act
        $this->post('register', $data);

        $user = User::whereEmail($data['email'])->first();
        $user->subscription('main')->cancel();
        $user->subscription('main')->resume();

        // assert
        $subscription = Subscription::whereUserId($user->id)->first();
        $this->assertNull( $subscription->ends_at);
    }
}
