<?php

namespace Tests\Unit;

use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CustomerRegistrationTest extends TestCase
{
    use DatabaseTransactions;

    public function test_a_customer_can_register_and_pay()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'payment_method' => 'pm_card_visa',
            'type_id' => UserType::CUSTOMER,
        ];

        // act
        $this->post('register', $data);

        // assert
        $this->assertDatabaseHas('users', [
            'name' => $data['name'],
            'email' => $data['email'],
            'card_brand' => 'visa',
            'card_last_four' => '4242',
            'type_id' => UserType::CUSTOMER,
        ]);
    }

    public function test_a_customer_can_opt_in_to_newsletter()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'payment_method' => 'pm_card_visa',
            'type_id' => UserType::CUSTOMER,
            'wants_newsletter' => 1,
        ];

        // act
        $this->post('register', $data);

        // assert
        $this->assertDatabaseHas('users', [
            'name' => $data['name'],
            'email' => $data['email'],
            'card_brand' => 'visa',
            'card_last_four' => '4242',
            'type_id' => UserType::CUSTOMER,
            'wants_newsletter' => 1,
        ]);
    }

    public function test_a_customer_can_opt_out_of_newsletter()
    {
        // arrange
        $data = [
            'name' => 'Shez Azr',
            'email' => 'shez@me.com',
            'password' => 'Password',
            'password_confirmation' => 'Password',
            'payment_method' => 'pm_card_visa',
            'type_id' => UserType::CUSTOMER,
            'wants_newsletter' => 0,
        ];

        // act
        $this->post('register', $data);

        // assert
        $this->assertDatabaseHas('users', [
            'name' => $data['name'],
            'email' => $data['email'],
            'card_brand' => 'visa',
            'card_last_four' => '4242',
            'type_id' => UserType::CUSTOMER,
            'wants_newsletter' => 0,
        ]);
    }
}
