<?php

namespace Tests\Unit;

use Tests\TestCase;

class HelpersTest extends TestCase
{
    public function test_formatted_duration_gives_hours_minutes_seconds()
    {
        // assert
        $this->assertEquals('1 hour', formatDuration(3600));
        $this->assertEquals('1 hour and 1 minute', formatDuration(3660));
        $this->assertEquals('1 hour and 12 minutes', formatDuration(4320));
        $this->assertEquals('30 minutes', formatDuration(1800));
        $this->assertEquals('24 hours and 10 minutes', formatDuration(87005));
    }
}
