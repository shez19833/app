<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Provider;
use App\Models\Series;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProvidersTest extends TestCase
{
    use DatabaseTransactions;

    public function test_saving_provider_saves_name_also_as_slug()
    {
        // arrange
        $p = factory(Provider::class)->make([
            'name' => "Who am I?"
        ]);
        $p->slug = null;

        // act
        $p->save();

        // assert
        $this->assertEquals('who-am-i', $p->slug);
    }

    public function test_i_can_get_an_episode_count_for_a_provider()
    {
        // arrange
        $p = factory(Provider::class)->create();

        for($i=0; $i<2; $i++) {
            $s = factory(Series::class)->create([
                'provider_id' => $p->id,
            ]);

            factory(Episode::class, 10)->create([
                'series_id' => $s,
            ]);
        }

        // act
        $p->loadCount('episodes');

        // assert
        $this->assertEquals(20, $p->episodes_count);
    }
}
