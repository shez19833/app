<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Payment;
use App\Models\Provider;
use App\Models\Series;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Subscription;
use Tests\TestCase;

class PayoutsCreatingTest extends TestCase
{
    use DatabaseTransactions;

    protected $customers;
    protected $episodes;
    protected $totalAmountPaid;

    public function setUp(): void
    {
        parent::setUp();

        foreach (['Provider 1', 'Provider 2', 'Provider 3'] as $name) {
            $user = factory(User::class)
                ->states('provider')
                ->create(['name' => $name]);

            $provider = factory(Provider::class)
                ->create(['user_id' => $user->id]);

            $series = factory(Series::class)
                ->create(['provider_id' => $provider->id]);

            factory(Episode::class, 10)
                ->create(['series_id' => $series->id]);
        }

        for ($i = 0; $i < 5; $i++) {
            $customer = factory(User::class)
                ->states('customer')
                ->create();

            factory(Subscription::class)
                ->create(['user_id' => $customer->id]);

            factory(Payment::class)
                ->states('lastMonth')
                ->create([
                    'total' => Setting::get(Setting::SUBSCRIPTION_PRICE),
                    'user_id' => $customer->id
                ]);

            $this->customers[] = $customer;
        }

        $this->totalAmountPaid = Setting::get(Setting::SUBSCRIPTION_PRICE) * 5;
        $this->episodes['total'] = 30;
    }

    public function test_system_creates_payout_using_watched_content_if_watched_anything()
    {
        // arrange
        Provider::with('series.episodes')
            ->get()
            ->each(function ($provider){
                $episodes = $provider->episodes
                    ->random(5)
                    ->pluck('id');

            for ($i = 0; $i <= 2; $i++) {
                $this->customers[$i]->watched()->sync($episodes, false);
            }
        });

        $this->episodes['watched'] = 45;

//        dump(Episode::count());
//        dd(DB::SELECT('select count(*) from watched'));

        DB::update("UPDATE watched
               SET watched_at = '" . now()->subMonth() . "'");

        // act
        $this->artisan('z:workout-payouts');

        // assert
        $this->assertDatabaseHas('payouts', [
           'user_id' => 1,
           'total' => $this->totalAmountPaid / $this->episodes['watched'] * 15,
           'period' => now()->subMonth()->endOfMonth(),
           'paid_at' => null,
        ]);
    }

    public function test_system_creates_equal_payout_for_providers_if_no_content_watched()
    {
        $this->artisan('z:workout-payouts');

        // assert
        $this->assertDatabaseHas('payouts', [
            'user_id' => 1,
            'total' => $this->totalAmountPaid / 3,
            'period' => now()->subMonth()->endOfMonth(),
            'paid_at' => null,
        ]);
    }
}
