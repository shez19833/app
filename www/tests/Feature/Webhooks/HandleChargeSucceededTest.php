<?php

namespace Tests\Feature\Webhooks;

use App\Http\Controllers\WebhookController;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class HandleChargeSucceededTest extends TestCase {

    use DatabaseTransactions;

    public function test_payments_created_when_a_hook_is_incoming()
    {
        // arrange
        $payload = json_decode('{
          "created": 1326853478,
          "livemode": false,
          "id": "evt_00000000000000",
          "type": "charge.succeeded",
          "object": "event",
          "request": null,
          "pending_webhooks": 1,
          "api_version": "2019-09-09",
          "data": {
            "object": {
              "id": "ch_00000000000000",
              "object": "charge",
              "amount": 6000,
              "amount_refunded": 0,
              "application": null,
              "application_fee": null,
              "application_fee_amount": null,
              "balance_transaction": "txn_00000000000000",
              "billing_details": {
                "address": {
                  "city": null,
                  "country": null,
                  "line1": null,
                  "line2": null,
                  "postal_code": null,
                  "state": null
                },
                "email": null,
                "name": null,
                "phone": null
              },
              "captured": true,
              "created": 1513457468,
              "currency": "gbp",
              "customer": "cus_00000000000000",
              "description": null,
              "destination": null,
              "dispute": null,
              "failure_code": null,
              "failure_message": null,
              "fraud_details": {
              },
              "invoice": "in_00000000000000",
              "livemode": false,
              "metadata": {
              },
              "on_behalf_of": null,
              "order": null,
              "outcome": {
                "network_status": "approved_by_network",
                "reason": null,
                "risk_level": "normal",
                "seller_message": "Payment complete.",
                "type": "authorized"
              },
              "paid": true,
              "payment_intent": null,
              "payment_method": "card_00000000000000",
              "payment_method_details": {
                "card": {
                  "brand": "visa",
                  "checks": {
                    "address_line1_check": null,
                    "address_postal_code_check": null,
                    "cvc_check": null
                  },
                  "country": "US",
                  "exp_month": 11,
                  "exp_year": 2020,
                  "fingerprint": "4YxPEqDK23RcBBcG",
                  "funding": "credit",
                  "installments": null,
                  "last4": "4242",
                  "network": "visa",
                  "three_d_secure": null,
                  "wallet": null
                },
                "type": "card"
              },
              "receipt_email": null,
              "receipt_number": null,
              "receipt_url": "https://pay.stripe.com/receipts/acct_1BYjndFwmNhgzywl/ch_1BZmciFwmNhgzywlQEbHOZgF/rcpt_EHROy2tJy8ruuWZzmSsDd2ZqAlgv7UO",
              "refunded": false,
              "refunds": {
                "object": "list",
                "data": [
                ],
                "has_more": false,
                "total_count": 0,
                "url": "/v1/charges/ch_1BZmciFwmNhgzywlQEbHOZgF/refunds"
              },
              "review": null,
              "shipping": null,
              "source": {
                "id": "card_00000000000000",
                "object": "card",
                "address_city": null,
                "address_country": null,
                "address_line1": null,
                "address_line1_check": null,
                "address_line2": null,
                "address_state": null,
                "address_zip": null,
                "address_zip_check": null,
                "brand": "Visa",
                "country": "US",
                "customer": "cus_00000000000000",
                "cvc_check": null,
                "dynamic_last4": null,
                "exp_month": 11,
                "exp_year": 2020,
                "fingerprint": "4YxPEqDK23RcBBcG",
                "funding": "credit",
                "last4": "4242",
                "metadata": {
                },
                "name": null,
                "tokenization_method": null
              },
              "source_transfer": null,
              "statement_descriptor": "Keepussafe - Yearly",
              "statement_descriptor_suffix": null,
              "status": "succeeded",
              "transfer_data": null,
              "transfer_group": null
            }
          }
        }', true);

        $user = factory(User::class)->create();
        $user->stripe_id = 'cus_00000000000000';
        $user->save();

        // act
//         $this->post('stripe/webhook', $payload);
        $controller = new WebhookController();
        $controller->handleChargeSucceeded($payload);

        // assert
        $this->assertDatabaseHas('payments', [
            'total' => 6000,
            'user_id' => $user->id,
            'paid_at' => Carbon::createFromTimestamp(1513457468),
        ]);
    }
}
