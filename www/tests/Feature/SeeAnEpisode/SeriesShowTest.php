<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Series;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SeriesShowTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_see_a_series_with_its_episode()
    {
        // arrange
        $series = factory(Series::class)->create([
            'name' => 'PHP Rocks',
            'slug' => 'php-rocks',
        ]);

        factory(Episode::class)->create([
            'series_id' => $series->id,
            'name' => 'Chapter 1',
            'slug' => 'chapter-1',
            'description' => 'Chapter 1 description',
            'duration' => 90,
        ]);

        // act
        $this->get('series/php-rocks/episodes/chapter-1')

            // assert
            ->assertSee('PHP Rocks')
            ->assertSee('Chapter 1')
            ->assertSee('Chapter 1 description')
            ->assertSee(90);

        // @todo video link
        // change duration appropriately
    }
}
