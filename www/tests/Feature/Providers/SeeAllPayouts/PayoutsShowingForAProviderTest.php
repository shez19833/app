<?php

namespace Tests\Unit;

use App\Models\Payout;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PayoutsShowingForAProviderTest extends TestCase
{
    use DatabaseTransactions;

    public function test_user_can_see_past_payouts_with_recent_first()
    {
        // arrange
        $user = factory(User::class)
            ->states('provider')
            ->create();

        factory(Payout::class)
            ->state('twoMonthsAgo')
            ->create([
                'user_id' => $user->id,
                'total' => 30000
            ]);

        factory(Payout::class)
            ->state('lastMonth')
            ->create([
                'user_id' => $user->id,
                'total' => 10000
            ]);

        // act
        $this->actingAs($user)
            ->get('payouts')

            // assert
//            ->assertSeeInOrder(['£100', '£300']);
            ->assertSeeInOrder([10000, 30000]);
    }
}
