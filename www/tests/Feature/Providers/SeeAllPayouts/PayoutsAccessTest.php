<?php

namespace Tests\Unit;

use App\Models\Provider;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PayoutsAccessTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_get_to_payouts_page_if_provider()
    {
        // arrange
        $user = factory(User::class)
            ->states('provider')
            ->create();

        // act
        $this->actingAs($user)
            ->get('payouts')

            // assert
            ->assertStatus(200);
    }

    public function test_i_cannot_get_to_payouts_page_if_customer()
    {
        // arrange
        $user = factory(User::class)
            ->states('customer')
            ->create();

        // act
        $this->actingAs($user)
            ->get('payouts')

            // assert
            ->assertRedirect('/');
    }

    public function test_i_cannot_get_to_payouts_page_if_not_logged()
    {
        // act
        $this->get('payouts')

            // assert
            ->assertRedirect('/login');
    }
}
