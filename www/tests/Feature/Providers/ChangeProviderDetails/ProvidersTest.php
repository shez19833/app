<?php

namespace Tests\Feature;

use App\Models\Episode;
use App\Models\Provider;
use App\Models\Series;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ProvidersTest extends TestCase
{
    use DatabaseTransactions;

    public function test_providers_can_change_their_provider_details()
    {
        // arrange
        $user = factory(User::class)
            ->states('provider')
            ->create([
                'name' => 'old name',
            ]);

        factory(Provider::class)
            ->create([
                'user_id' => $user->id,
            ]);

        // act
        $this->actingAs($user)
            ->post('account', [
                'provider_name' => 'New Name',
                'provider_description' => 'New Description',
                'provider_url' => 'http://newURL.com',
        ]);

        // assert
        $this->assertDatabaseHas('providers', [
            'user_id' => $user->id,
            'name' => 'New Name',
            'description' => 'New Description',
            'url' => 'http://newURL.com',
            'slug' => 'new-name',
        ]);
    }
}
