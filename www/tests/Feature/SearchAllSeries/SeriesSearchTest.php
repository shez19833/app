<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Provider;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SeriesSearchTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_search_series_by_a_provider()
    {
        // arrange
        $data = [
            'Shez AC' => 'PHP',
            'Tom PC' => 'Java',
            'Chris PC' => 'Ruby'
        ];

        foreach ($data as $provider => $seriesName) {
            $provider = factory(Provider::class)->create([
                'name' => $provider,
            ]);

            $series = factory(Series::class)->create([
                'provider_id' => $provider->id,
                'name' => $seriesName . ' TDD',
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?providers[]=Shez AC')

            // assert
            ->assertSee('PHP TDD')
            ->assertDontSee('Java TDD')
            ->assertDontSee('Ruby TDD');
    }

    public function test_i_can_search_series_by_multiple_provider()
    {
        // arrange
        $data = [
            'Shez AC' => 'PHP',
            'Tom PC' => 'Java',
            'Chris PC' => 'Ruby'
        ];

        foreach ($data as $provider => $seriesName) {
            $provider = factory(Provider::class)->create([
                'name' => $provider,
            ]);

            $series = factory(Series::class)->create([
                'provider_id' => $provider->id,
                'name' => $seriesName . ' TDD',
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?providers[]=Shez AC&providers[]=Tom PC')

            // assert
            ->assertSee('PHP TDD')
            ->assertSee('Java TDD')
            ->assertDontSee('Ruby TDD');
    }

    public function test_i_can_search_series_by_exact_text()
    {
        // arrange
        foreach (['PHP Rocks', 'ASP Rocks', 'Java world'] as $name) {
            $series = factory(Series::class)->create([
                'name' => $name
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?text=PHP Rocks')

            // assert
            ->assertSee('PHP Rocks')
            ->assertDontSee('ASP Rocks')
            ->assertDontSee('Java world');
    }

    public function test_i_can_search_series_by_partial_text()
    {
        // arrange
        foreach (['PHP Rocks', 'ASP Rocks', 'Java world'] as $name) {
            $series = factory(Series::class)->create([
                'name' => $name
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?text=Rocks')

            // assert
            ->assertSee('PHP Rocks')
            ->assertSee('ASP Rocks')
            ->assertDontSee('Java world');
    }

    public function test_i_can_search_series_by_a_single_tag()
    {
        // arrange
        foreach (['php', 'java'] as $tag){
            $tagModel = factory(Tag::class)->create([
                'name' => $tag
            ]);

            $series = factory(Series::class)->create([
                'name' => $tag . ' Hard way'
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);

            $series->tags()->sync([$tagModel->id]);
        }

        // act
        $this->get('?tags[]=php')

            // assert
            ->assertSee('php Hard way')
            ->assertDontSee('java Hard way');
    }

    public function test_i_can_search_series_by_multiple_tags()
    {
        // arrange
        foreach (['php', 'laravel', 'java'] as $tag){
            $tagModel = factory(Tag::class)->create([
                'name' => $tag
            ]);

            $series = factory(Series::class)->create([
                'name' => $tag . ' Hard way'
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);

            $series->tags()->sync([$tagModel->id]);
        }

        // act
        $this->get(sprintf('?tags[]=%s&tags[]=%s',
            'php', 'laravel'))

            // assert
            ->assertSee('php Hard way')
            ->assertSee('laravel Hard way')
            ->assertDontSee('java Hard way');
    }

    public function test_series_is_sorted_by_name_automatically()
    {
        // arrange
        foreach (['php', 'java', 'c'] as $seriesName) {
            $series = factory(Series::class)->create([
                'name' => $seriesName . ' Hard way '
            ]);

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('/')

            // assert
            ->assertSeeInOrder(['c Hard way', 'java Hard way', 'php Hard way']);
    }

    public function test_i_can_sort_series_by_date_updated()
    {
        // arrange
        foreach (range(3,1) as $date) {
            $series = factory(Series::class)->make([
                'name' => 'Hard way ' . $date
            ]);

            $series->created_at = now()->subDays($date);
            $series->updated_at = now()->subDays($date);
            $series->save();

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?order=recent')

            // assert
            ->assertSeeInOrder(['Hard way 1', 'Hard way 2', 'Hard way 3']);
    }

    public function test_i_can_search_by_text_and_sort_series_by_date_updated()
    {
        // arrange
        foreach (range(3,1) as $date) {
            $series = factory(Series::class)->make([
                'name' => 'Hard way ' . $date
            ]);

            $series->created_at = now()->subDays($date);
            $series->updated_at = now()->subDays($date);
            $series->save();

            factory(Episode::class)->create([
                'series_id' => $series->id,
            ]);
        }

        // act
        $this->get('?order=recent')

            // assert
            ->assertSeeInOrder(['Hard way 1', 'Hard way 2', 'Hard way 3']);
    }
}
