<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Provider;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SeriesAllTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_see_paginated_series()
    {
        // arrange
        $data = [
            'name' => 'PHP TDD',
            'description' => 'Awesome Test Book',
            'slug' => 'php-tdd',
        ];

        $series = factory(Series::class)->create($data);

        $episode = factory(Episode::class)->create([
            'series_id' => $series->id,
        ]);

        // act
        $this->get('/')

            // assert
            ->assertSee($data['name'])
            ->assertSee(route('seriesEpisodes.show', [
                'seriesSlug' => $series->slug,
                'episodeSlug' => $episode->slug,
            ]));

        // @todo duration in seconds or minutes/hours
        // @todo maybe show provider name :/
    }
}
