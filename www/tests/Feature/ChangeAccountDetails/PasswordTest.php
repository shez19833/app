<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class PasswordTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_change_password_if_current_password_is_valid()
    {
        // arrange
        $user = factory(User::class)->create([
            'email' => 'me@me.com'
        ]);

        // act
        $this->actingAs($user)
            ->post('account', [
                'old_password' => 'password',
                'new_password' => 'P@ssw0rd',
                'new_password_confirmation' => 'P@ssw0rd',
            ]);

        // assert
        $user = User::find($user->id);

        $this->assertTrue(Hash::check('P@ssw0rd', $user->password));
    }

    public function test_i_cant_change_password_if_current_password_is_invalid()
    {
        // arrange
        $user = factory(User::class)->create([
            'email' => 'me@me.com'
        ]);

        // act
        $this->actingAs($user)
            ->post('account', [
                'old_password' => 'invalidPassword',
                'new_password' => 'P@ssw0rd',
                'new_password_confirmation' => 'P@ssw0rd',
            ]);

        // assert
        $user = User::find($user->id);

        $this->assertFalse(Hash::check('password1', $user->password));
    }

    public function test_i_cant_change_password_if_new_password_doesnt_match_confirmation_password()
    {
        // arrange
        $user = factory(User::class)->create([
            'email' => 'me@me.com'
        ]);

        // act
        $this->actingAs($user)
            ->post('account', [
                'old_password' => 'password',
                'new_password' => 'P@ssw0rd',
                'new_password_confirmation' => 'P@ssw0rd2',
            ]);

        // assert
        $user = User::find($user->id);

        $this->assertFalse(Hash::check('password1', $user->password));
    }
}
