<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class DetailsTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_change_name()
    {
        // arrange
        $user = factory(User::class)->create([
            'name' => 'old name',
        ]);

        // act
        $this->actingAs($user)
            ->post('account', ['name' => 'new name']);


        // assert
        $this->actingAs($user)
            ->get('account')
            ->assertSee('new name')
            ->assertDontSee('old name');

    }

    public function test_i_can_change_email()
    {
        // arrange
        $user = factory(User::class)->create([
            'email' => 'me@me.com'
        ]);

        // act
        $this->actingAs($user)
            ->post('account', ['email' => 'me@you.com']);


        // assert
        $this->actingAs($user)
            ->get('account')
            ->assertSee('me@you.com')
            ->assertDontSee('me@me.com');

    }
}
