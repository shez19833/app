<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class NewsletterTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_opt_in_to_newsletter()
    {
        // arrange
        $user = factory(User::class)->create([
            'name' => 'old name',
            'wants_newsletter' => 0,
        ]);

        // act
        $this->actingAs($user)
            ->post('account', ['wants_newsletter' => 1]);


        // assert
        $this->assertTrue(User::find($user->id)->wants_newsletter);

    }

    public function test_i_can_opt_out_of_newsletter()
    {
        // arrange
        $user = factory(User::class)->create([
            'name' => 'old name',
            'wants_newsletter' => 1,
        ]);

        // act
        $this->actingAs($user)
            ->post('account', ['wants_newsletter' => 0]);


        // assert
        $this->assertFalse(User::find($user->id)->wants_newsletter);

    }
}
