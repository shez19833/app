<?php

namespace Tests\Unit;

use App\Models\Provider;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccessTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_get_to_account_page_if_customer()
    {
        // arrange
        $user = factory(User::class)
            ->states('customer')
            ->create();

        // act
        $this->actingAs($user)
            ->get('account')

        // assert
            ->assertSee($user->name);
    }

    public function test_i_get_to_account_page_if_provider()
    {
        // arrange
        $user = factory(User::class)
            ->states('provider')
            ->create();

        factory(Provider::class)
            ->create(['user_id' => $user->id]);

        // act
        $this->actingAs($user)
            ->get('account')

            // assert
            ->assertSee($user->name);
    }

    public function test_i_cannot_get_to_account_page_if_not_logged()
    {
        // act
        $this->get('account')

            // assert
            ->assertRedirect('/login');
    }
}
