<?php

namespace Tests\Unit;

use App\Models\Episode;
use App\Models\Series;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class WatchAnEpisodeTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_watch_an_episode()
    {
        // arrange
        $series = factory(Series::class)->create([
            'name' => 'PHP Rocks',
            'slug' => 'php-rocks',
        ]);

        $episodes = [];
        for ($i = 1; $i < 4; $i++) {
            $episodes[] = factory(Episode::class)->create([
                'series_id' => $series->id,
                'name' => 'Chapter ' . $i,
                'description' => 'Chapter '. $i .' description',
                'duration' => 90 * $i,
                'slug' => 'chapter-' . $i,
            ]);
        }

        // act
        $this->get('series/php-rocks/episodes/chapter-2')

            // assert
            ->assertSee('PHP Rocks')

            // prev/next
            ->assertSee('series/php-rocks/episodes/chapter-1')
            ->assertSee('series/php-rocks/episodes/chapter-3')

            ->assertSee('Chapter 1')
            ->assertSee('Chapter 1 description')
            ->assertSee(90)

            ->assertSee('Chapter 2')
            ->assertSee('Chapter 2 description')
            ->assertSee(180)

            ->assertSee('Chapter 2')
            ->assertSee('Chapter 2 description')
            ->assertSee(270);

        // @todo video link
        // change duration appropriately
    }
}
