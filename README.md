# Functionality

## Bugs

 - If card requires 3ds even if u do it through JS PHP moans?
 - need to work out the inconsistencies ie. one user starts at middle of month do we figure this out whilst giving a payout?
    - did it by recording payments when they happen on stripe! 
 
## Providers
 - (x) Registration 
 - (x) Login
 - (x) Logout
 - (x) Forgotten Password
 - (x) Email Verification
 - (x) Allow user to change account details
    - (x) Allow user to change their name / email / passwords
    - (x) Allow user to opt-in/out of newsletters
    - (x) Allow user to change their Provider info
 - (x) Payout
    - (x) See List of Past Payouts (Date: Amount Paid)  
 
## User 
 - (x) Registration (Including Stripe/Subscription)
 - (x) Login
 - (x) Logout
 - (x) Forgotten Password
 - (x) Email Verification
 - (x) See All Providers 
 - (x) See All Series
     - (x) Allow user to search
        - (x) Search by name
        - (x) Search by tag(s)
        - (x) Search by provider(s)
     - (x) Allow user to sort series
        - By Date (latest first)   
 - (x) See All Episodes of a Series (x)
 - (x) Allowing user to watch an episode
 - Allow user to mark an episode as 'watched'
 - (x) Allow user to change account details
   - (x) Allow user to change their name / email / passwords
   - (x) Allow user to cancel their subscription
   - (x) Allow user to change their card
   - (x) Allow user to renew their subscription
   - (x) Allow user to opt-in/out of newsletters

## System 
 - (x) Email users about cancelling Cards (Stripe, need testing)
 - (x) Email users about Failed Payment (Stripe, need testing)
 - (x) Stripe Hooks:
   - (x) customer.subscription.updated (BY cashier)
   - (x) customer.subscription.deleted (BY cashier)
   - (x) customer.updated (BY cashier)
   - (x) customer.deleted (BY cashier)
   - (x) invoice.payment_action_required (BY cashier)
   - (x) Charge Succeeded
 - Take monthly payments from users (Stripe, need testing)
   - (x) Charge user (automatically by Stripe, need testing)
   - Send Monthly confirmation of payment taken (?)
   - (x) Send Email in case of Payment Problem (Stripe)
   - Cancel Subscription after x amount of days if not paid
    - (x) Stripe does it automatically (test)
    - (x) we have to update our systems through webhook
 - (x) Scrape Providers
   - (x) LC
   - (x) CC
   - Alternate between all Providers (dont do every provider every day, assign a day for each)
   - Look at each series & their episode count & the scraped episode count when scraping and only scrape if its different. Have a scrape date when each series is scraped (only if there are new episodes)
   - Run at midnight and have a little sleep to avoid detection
 - (x) Pay out
   - (x) Every Provider gets a payout at the start of the month for the last month.
   - (x) Work out how much to give them depending on episodes watched.
   
## Things to do:
 - Design Pages nice
 - Refactor to make things clean
 - Tests - change so more easier
    - Mocking etc... so we have integration tests separate
 - Videos from Providers & 'making them watched automatically'
    - I could time the duration user is on a page and then compare with duration? do a js call every 30 sec or so?  
 - Test everything from each persons perspective
 - Laracasts etc - also try to capture beginner/difficulty
 - When provider registers provider table needs to be filled.. atm its empty
   
Create Users:

1. With valid card so when it is charged again it passes
2. With invalid card so it gives problem on renewal
3. With 3DS secure thing so it prompts uer to 'verify'